<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >

    <xsl:variable name="maxImageHeight" select="15 * 0.5 * 29"/>
    <xsl:variable name="maxImageWidth" select="11 * 0.9 * 29"/>
    <xsl:variable name="maxAspectRatio" select="$maxImageWidth div $maxImageHeight"/>
    
    <xsl:variable name="mainFont" select="8"/>
    <xsl:variable name="titleFont" select="12"/>
    <xsl:variable name="headerTitleFont" select="16"/>
    <xsl:variable name="footerFont" select="4"/>
    <xsl:variable name="spaceAfter" select="4"/>
    
    <xsl:template match="Crag">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="first-page" page-height="15cm" page-width="11cm" margin-top=".5cm" margin-bottom="0.5cm" margin-left="0.5cm" margin-right="0.5cm">
                    <fo:region-body region-name="xsl-region-body"/>
                    <fo:region-after region-name="first-page-footer"/>
                </fo:simple-page-master>
                <fo:simple-page-master master-name="simpleA6" page-height="15cm" page-width="11cm" margin-top=".5cm" margin-bottom="0.5cm" margin-left="0.5cm" margin-right="0.5cm">
                    <fo:region-body region-name="xsl-region-body"/>
                    <fo:region-before region-name="page-header"/>
                    <fo:region-after region-name="page-footer"/>
                </fo:simple-page-master>
                <fo:page-sequence-master master-name="my-pages">
                    <fo:repeatable-page-master-alternatives>
                        <fo:conditional-page-master-reference page-position="first" master-reference="first-page"/>
                        <fo:conditional-page-master-reference master-reference="simpleA6"/>
                    </fo:repeatable-page-master-alternatives>
                </fo:page-sequence-master>
            </fo:layout-master-set>
 
            <fo:page-sequence master-reference="my-pages">
                <fo:static-content flow-name="first-page-footer">
                    <fo:block font-family='Helvetica' font-size="{$footerFont}">
                        <xsl:value-of select="License"/>
                    </fo:block>
                </fo:static-content>
                <fo:static-content flow-name="page-footer">
                    <fo:block font-family='Helvetica' font-size="{$footerFont}" text-align-last="justify" span="all">
                        © <xsl:value-of select="@topoAuthors"/>
                        <fo:leader vertical-align="top" leader-pattern="space"/>
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-family='Helvetica' span="all">
                        <fo:table >
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block font-size="{$headerTitleFont}" font-weight="bold">
                                            <xsl:value-of select="@name"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block>
                                            <fo:table >
                                                <fo:table-body>
                                                    <fo:table-row>
                                                        <fo:table-cell>
                                                            <fo:block font-size="{$mainFont}" text-align="right">
                                                                <xsl:value-of select="@date"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                    <fo:table-row>
                                                        <fo:table-cell>
                                                            <fo:block font-size="{$mainFont}" text-align="right">
                                                                © <xsl:value-of select="@topoAuthors"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                </fo:table-body>
                                            </fo:table>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                    <fo:block font-family='Helvetica' text-align-last="justify" span="all">
                        <fo:leader vertical-align="top" leader-pattern="rule"/>
                    </fo:block>
                    
                    <xsl:apply-templates select="Text"/>
                    <xsl:apply-templates select="Area"/>
                </fo:flow>      
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template match="Text">
        <xsl:if test="@showTitle!='false'">
            <fo:block font-size="{$titleFont}" font-weight="bold" span="all" >    
                <xsl:value-of select="@name"/>
            </fo:block>
        </xsl:if>
        <xsl:if test=".!=''">
            <fo:block font-size="{$mainFont}" text-align="justify" span="all" space-after="{$spaceAfter}">
                <xsl:value-of select="."/>
            </fo:block>
        </xsl:if>
        <xsl:if test="@image!=''">
            <fo:block text-align="center" span="all" >
                <fo:external-graphic src="{@image}" max-width="90%" max-height="{$maxImageHeight}cm" content-height="scale-down-to-fit" content-width="scale-down-to-fit"/>
            </fo:block>
            <fo:block font-size="{$mainFont}" text-align="center" span="all" space-after="{$spaceAfter}">
                <xsl:value-of select="@caption"/>
            </fo:block>
        </xsl:if>
    </xsl:template>
    <xsl:template match="Area">
        <fo:block font-size="{$titleFont}" font-weight="bold" span="all" keep-with-next="always">    
            <xsl:value-of select="@name"/>
        </fo:block>
        <xsl:if test="Description!=''">
            <fo:block font-size="{$mainFont}" text-align="justify" span="all" keep-with-next="always">
                <xsl:value-of select="Description"/>
            </fo:block>
        </xsl:if>
            
        <xsl:apply-templates select="TopoImage"/>
        
        <fo:block text-align="center" span="all" >
            <fo:leader/>
        </fo:block>
    </xsl:template>
    <xsl:template match="TopoImage">
        <fo:block text-align="center" span="all" space-after="{$spaceAfter}">
            <xsl:variable name="aspectRatio" select="@width div @height"/>
            <xsl:variable name="width">
                <xsl:choose>
                    <xsl:when test="$aspectRatio &gt; $maxAspectRatio">
                        <xsl:value-of select="$maxImageWidth"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$maxImageWidth * $aspectRatio"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <!--Use 100% width below for consistancy with the text columns-->
            <fo:instream-foreign-object max-height="{$maxImageHeight}" content-height="scale-down-to-fit" max-width="100%" content-width="scale-down-to-fit">
                <svg:svg width="{$width}" height="{$width div $aspectRatio}">
                    <!--<svg:svg width="17cm" height="15cm">-->
                    <svg:svg width="100%" height="100%" viewBox="0 0 {@width} {@height}">
                        <svg:image xlink:href="{@image}" width="100%" height="100%"/>
                    </svg:svg>
                    <xsl:apply-templates select="TopoLine" mode="svgLine"/>
                    <xsl:apply-templates select="TopoLine" mode="svgStart"/>
                </svg:svg>
            </fo:instream-foreign-object>
        </fo:block>
                
        <fo:list-block text-align="justify" font-size="{$mainFont}" space-before="{$spaceAfter}" space-after="{$spaceAfter}">
            <xsl:apply-templates select="Route"/>
        </fo:list-block>
    </xsl:template>
    <xsl:template match="Route">
        <fo:list-item space-after="{$spaceAfter}">
            <fo:list-item-label end-indent="label-end()">
                <xsl:variable name="colourMod" select="@count mod 9"/>
                <xsl:variable name="colour">
                    <xsl:choose>
                        <xsl:when test="$colourMod=1">red</xsl:when>
                        <xsl:when test="$colourMod=2">blue</xsl:when>
                        <xsl:when test="$colourMod=3">yellow</xsl:when>
                        <xsl:when test="$colourMod=4">green</xsl:when>
                        <xsl:when test="$colourMod=5">magenta</xsl:when>
                        <xsl:when test="$colourMod=6">purple</xsl:when>
                        <xsl:when test="$colourMod=7">lime</xsl:when>
                        <xsl:when test="$colourMod=8">pink</xsl:when>
                        <xsl:when test="$colourMod=0">orange</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <fo:block color="{$colour}" font-weight="bold">
                    <xsl:value-of select="@count"/>.
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body >
                <fo:block text-align-last="justify" start-indent="body-start()">
                    <fo:inline font-weight="bold">
                        <xsl:value-of select="@name"/> &#160;
                    </fo:inline>
                    <fo:inline font-style="italic">
                        <xsl:value-of select="@grade"/>
                    </fo:inline>
                    <fo:leader leader-pattern="space"/>
                    <xsl:if test="@project='true'">
                        <fo:inline font-style="italic">
                            (Project)
                        </fo:inline>
                    </xsl:if>
                    <xsl:if test="(@project='false') and (@faYear!='')">
                        (<xsl:value-of select="@faYear"/>)
                    </xsl:if>
                </fo:block>
                <fo:block font-size="{$mainFont}" text-align="justify" keep-with-previous.within-column="always">
                    <xsl:value-of select="Description"/>      
                </fo:block>
                <xsl:if test="(@project='false') and (@fa!='')">
                    <fo:block font-size="{$mainFont} * 0.8" text-align="justify">
                        FA: <xsl:value-of select="@fa"/>      
                    </fo:block>
                </xsl:if>   
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>
    <xsl:template match="TopoLine" mode="svgLine">   
        <xsl:variable name="colourMod" select="@count mod 9"/>
        <xsl:variable name="colour">
            <xsl:choose>
                <xsl:when test="$colourMod=1">red</xsl:when>
                <xsl:when test="$colourMod=2">blue</xsl:when>
                <xsl:when test="$colourMod=3">yellow</xsl:when>
                <xsl:when test="$colourMod=4">green</xsl:when>
                <xsl:when test="$colourMod=5">magenta</xsl:when>
                <xsl:when test="$colourMod=6">purple</xsl:when>
                <xsl:when test="$colourMod=7">lime</xsl:when>
                <xsl:when test="$colourMod=8">pink</xsl:when>
                <xsl:when test="$colourMod=0">orange</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <svg:svg viewBox="0 0 100 100" preserveAspectRatio="none">
            <xsl:variable name="aspectRatio" select="../@width div ../@height"/>
            <xsl:variable name="width">
                <xsl:choose>
                    <xsl:when test="$aspectRatio &gt; $maxAspectRatio">
                        <xsl:value-of select="$maxImageWidth"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$maxImageWidth * $aspectRatio"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="height" select="$width div $aspectRatio"/>
            <xsl:variable name="stroke" select="((150 div $width) + (150 div $height)) div 2"/>
            <svg:path d="M {@x1} {@y1} C {@line}" stroke="{$colour}" stroke-width="{$stroke}" fill="none" vector-effect="non-scaling-stroke"/>
        </svg:svg>
        <svg:circle style="fill:{$colour};stroke:{$colour}" stroke-width="1" cx="{@x2}%" cy="{@y2}%" r="1.5" vector-effect="non-scaling-stroke"/>
    </xsl:template>
    <xsl:template match="TopoLine" mode="svgStart">   
        <xsl:variable name="colourMod" select="@count mod 9"/>
        <xsl:variable name="colour">
            <xsl:choose>
                <xsl:when test="$colourMod=1">red</xsl:when>
                <xsl:when test="$colourMod=2">blue</xsl:when>
                <xsl:when test="$colourMod=3">yellow</xsl:when>
                <xsl:when test="$colourMod=4">green</xsl:when>
                <xsl:when test="$colourMod=5">magenta</xsl:when>
                <xsl:when test="$colourMod=6">purple</xsl:when>
                <xsl:when test="$colourMod=7">lime</xsl:when>
                <xsl:when test="$colourMod=8">pink</xsl:when>
                <xsl:when test="$colourMod=0">orange</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="radius">
            <xsl:choose>
                <xsl:when test="string-length(@count) = 1">6</xsl:when>
                <xsl:otherwise>7</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <svg:circle style="fill:white;stroke:{$colour}" stroke-width="1.5" cx="{@x1}%" cy="{@y1}%" r="{$radius}pt" vector-effect="non-scaling-stroke"/>
        <svg:text x="{@x1}%" y="{@y1}%" text-anchor="middle" font-size="{$mainFont} * 0.8" dy="3" dominant-baseline="central" stroke-width="1" vector-effect="non-scaling-stroke"><xsl:value-of select="@count"/></svg:text>
    </xsl:template>
</xsl:stylesheet>
