/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import uk.destor.topos.data.parameters.Parameter;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class DiffParameter<E> extends Diff {
    private final Parameter<E> parameter;
    private E value;

    public DiffParameter(Diff parent, String name, Parameter<E> parameter, E value) {
       super(parent, name);

       if(parameter == null) throw new NullPointerException("parameter cannot be null.");

       this.parameter = parameter;
       this.value = value;
    }

    public Parameter<E> getParameter() {
       return parameter;
    }

    public void setValue(E value){
        this.value = value;
    }
    public E getValue() {
       return value;
    }

    public boolean hasChanged(){
       return !(parameter.getValue() == null ? value == null : parameter.getValue().equals(value));
    }

    @Override
    protected void acceptImpl() {
        parameter.setValue(value);
    }
}
