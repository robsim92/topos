/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoImage extends JLabel implements Serializable{
    private class ScalledImageIcon extends ImageIcon{
        public ScalledImageIcon(Image image) {
            super(image);
        }
        @Override
        public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
            if(getImageObserver() == null)
               g.drawImage(super.getImage(), 2, 2, TopoImage.this.getWidth()-4, TopoImage.this.getHeight()-4, c);
            else
               g.drawImage(super.getImage(), 2, 2, TopoImage.this.getWidth()-4, TopoImage.this.getHeight()-4, super.getImageObserver());
        }
    }
    private String imagePath;
    private Image image;
    private final Point imgSize = new Point();
    private float aspectRatio;
    
    private final List<TopoEntry> topoEntrys = new ArrayList<>();

    public TopoImage(String imagePath) {
        setBorder(new LineBorder(Color.BLACK));
        this.imagePath = imagePath;
    }
    public TopoImage(String imagePath, Image image) {
        setBorder(new LineBorder(Color.BLACK));
        this.imagePath = imagePath;
        setImage(image);
    }

    public final void setImage(Image image) {
        this.image = image;
        setIcon(new ScalledImageIcon(image));
        aspectRatio = image.getWidth(null)/(float)image.getHeight(null);
        imgSize.setLocation(image.getWidth(null), image.getHeight(null));
    }
    public Image getImage() {
        return image;
    }
    public Point getImgSize() {
        return imgSize;
    }
    public void setImageName(String imagePath) {
        this.imagePath = imagePath;
    }
    public String getImageName() {
        //Need to trim this to prevent problems on M$ systems due to whitespace at the end/start of the filenames
        return imagePath == null ? null : imagePath.trim();
    }
    public float getAspectRatio(){
        return aspectRatio;
    }
    public List<TopoEntry> getTopoEntrys() {
        return topoEntrys;
    }
}
