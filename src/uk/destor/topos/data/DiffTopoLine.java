/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class DiffTopoLine extends Diff{
    public enum Status{
        NEW{
            @Override
            public void accept(DiffTopoLine dtl) {
                dtl.topoEntry.getTopoLines().put(dtl.topoLine2.getTopoImage(), dtl.topoLine2);
            }
        },
        EXISTING_SAME{
            @Override
            public void accept(DiffTopoLine dtl) {}
        },
        EXISTING_CHANGED{
            @Override
            public void accept(DiffTopoLine dtl) {
                dtl.topoLine1.parseXml(dtl.topoLine2.assymptoteString());
                dtl.topoLine1.setTag(dtl.topoLine2.getTag());
            }
        },
        DELETE{
            @Override
            public void accept(DiffTopoLine dtl) {
                dtl.topoEntry.getTopoLines().remove(dtl.topoLine1.getTopoImage());
            }
            
        };
        public abstract void accept(DiffTopoLine dtl);
    }
    
    private final TopoLine topoLine1, topoLine2;
    private final TopoEntry topoEntry;
    private final Status status;

    public DiffTopoLine(TopoLine topoLine1, TopoLine topoLine2, TopoEntry topoEntry) {
        super("TopoLine - " + topoEntry.getName());
        this.topoLine1 = topoLine1;
        this.topoLine2 = topoLine2;
        this.topoEntry = topoEntry;
        
        if(topoLine1 == null) status = Status.NEW;
        else if(topoLine2 == null) status = Status.DELETE;
        else if(topoLine1.assymptoteString().equals(topoLine2.assymptoteString())) status = Status.EXISTING_SAME;
        else status = Status.EXISTING_CHANGED;
    }

    public Status getStatus() {
        return status;
    }
    
    @Override
    protected void acceptImpl() {
        status.accept(this);
    }

    public TopoEntry getTopoEntry(){
        return topoEntry;
    }
    public TopoLine getTopoLine1() {
        return topoLine1;
    }
    public TopoLine getTopoLine2() {
        return topoLine2;
    }
}
