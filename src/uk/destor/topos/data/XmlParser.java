/*
 * Copyright (C) 2015 Rob Sim <robsim91 at users.sourceforge.net>.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Rob Sim <robsim91 at users.sourceforge.net>.
 */
public abstract class XmlParser extends DefaultHandler{
    private class Reader implements XMLReader {
        private final XmlParser root;
        private ContentHandler contentHandler;
        
        private ErrorHandler errorHandler = null;
        private EntityResolver entityResolver = null;
        private DTDHandler dTDHandler = null;
        
        private final Map<String, Object> properties = new HashMap<>();
        private final Map<String, Boolean> features = new HashMap<>();
        
        public Reader(XmlParser root){
            this.root = root;
        }
        @Override
        public void parse(InputSource input) throws IOException, SAXException{
            if(contentHandler == null) throw new SAXException("No ContentHandler");
            
            contentHandler.startDocument();
            root.createXmlElement(contentHandler);
            contentHandler.endDocument();
        }
        @Override
        public void setErrorHandler(ErrorHandler handler){
            errorHandler = handler;
        }
        @Override
        public ErrorHandler getErrorHandler(){
            return errorHandler;
        }
        @Override
        public void setContentHandler(ContentHandler handler){
            contentHandler = handler;
        }
        @Override
        public ContentHandler getContentHandler(){
            return contentHandler;
        }
        @Override
        public void parse(String systemId) throws IOException, SAXException{
            System.out.println("Parse Called");
        }
        @Override
        public boolean getFeature(String name) throws SAXNotRecognizedException, SAXNotSupportedException{
            return features.get(name);
        }
        @Override
        public void setFeature(String name, boolean value) throws SAXNotRecognizedException, SAXNotSupportedException{
            features.put(name, value);
        }
        @Override
        public Object getProperty(String name) throws SAXNotRecognizedException, SAXNotSupportedException{
            return properties.get(name);
        }
        @Override
        public void setProperty(String name, Object value) throws SAXNotRecognizedException, SAXNotSupportedException{
            properties.put(name, value);
        }
        @Override
        public void setEntityResolver(EntityResolver resolver){
            entityResolver = resolver;
        }
        @Override
        public EntityResolver getEntityResolver(){
            return entityResolver;
        }
        @Override
        public void setDTDHandler(DTDHandler handler){
            dTDHandler = handler;
        }
        @Override
        public DTDHandler getDTDHandler(){
            return dTDHandler;
        }
    }
    private final String elementTag;
    
    
    protected String tag = null;
    private XmlParser nestedParser = null;
    protected StringBuilder builder = new StringBuilder(0);
    protected int rootCount;
    
    public XmlParser(String elementTag){
        this.elementTag = elementTag;
        if(this.elementTag == null) throw new NullPointerException("elementTag is null");
    }
    protected XmlParser startXmlElement(String uri, String localName, String qName, Attributes attributes){
        return null;
    }
    @SuppressWarnings( "NoopMethodInAbstractClass" )
    protected void elementRoot(String uri, String localName, String qName, Attributes attributes){}
    
    @SuppressWarnings( "NoopMethodInAbstractClass" )
    protected void endXmlElement(String uri, String localName, String qName){}
    public final  String getCharaters(){
        return builder.toString().trim();
    }
    
    public final <T extends XmlParser> T addToCollection(Collection<T> l, T t){
        l.add(t);
        return t;
    }
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException{
        if(getElementTag().equals(qName) && rootCount++ == 0){
            elementRoot(uri, localName, qName, attributes);
            return;
        }
        
        if(nestedParser == null){
            tag = qName;
            nestedParser = startXmlElement(uri, localName, qName, attributes);
            if(nestedParser != null) nestedParser.start(uri, localName, qName, attributes);
        }else nestedParser.startElement(uri, localName, qName, attributes);        
    }
    private void start(String uri, String localName, String qName, Attributes attributes) throws SAXException{
        rootCount = 0;
        tag = getElementTag();
        startElement(uri, localName, qName, attributes);
    }
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException{        
        if(getElementTag().equals(tag) && rootCount == 0) return;
        
        if(nestedParser == null) builder.append(ch, start, length);
        else nestedParser.characters(ch, start, length);
    }
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException{        
        if(getElementTag().equals(qName)) rootCount--;
        
        if(nestedParser == null){
            endXmlElement(uri, localName, qName);
            builder = new StringBuilder(0);
            tag = null;
        }else{
            nestedParser.endElement(uri, localName, qName);
            endXmlElement(uri, localName, qName);
            if(tag.equals(qName)) nestedParser = null;
        }
    }
    
    
    public String getElementTag(){
        return elementTag;
    }
    protected String namespaceUri(){
        return "";
    }
    protected Attributes elementAttributes(){
        return EMPTY_ATTRIBUTES;
    }
    
    protected void outputChildren(ContentHandler contentHandler) throws SAXException{}
    protected void outputChild(XmlParser xp, String tag, ContentHandler ch) throws SAXException{
        if(xp == null)
            createEmptyElement(ch, tag);
        else
            xp.createXmlElement(ch);
    }
    public final void createXmlElement(ContentHandler contentHandler) throws SAXException{
        contentHandler.startElement(namespaceUri(), getElementTag(), getElementTag(), elementAttributes());
        outputChildren(contentHandler);
        contentHandler.endElement(namespaceUri(), getElementTag(), getElementTag());
    }
    public final <T extends XmlParser> void createXmlElements(ContentHandler contentHandler, Collection<T> l) throws SAXException{
        for(XmlParser parser : l) parser.createXmlElement(contentHandler);
    }
    public final void createParsableElement(ContentHandler contentHandler, String name, Object o) throws SAXException{
        createStringElement(contentHandler, name, String.valueOf(o));
    }
    
    public final void createEmptyElement(ContentHandler contentHandler, String name) throws SAXException{
        contentHandler.startElement(namespaceUri(), name, name, new AttributesImpl());
        contentHandler.endElement(namespaceUri(), name, name);
    }
    public final void createStringElement(ContentHandler contentHandler, String name, String s) throws SAXException{
        if(name == null) throw new NullPointerException("name cannot be null");
        if(name.isEmpty()) throw new IllegalArgumentException("name cannot be empty");
        if(s == null || s.isEmpty()) return;
        
        contentHandler.startElement(namespaceUri(), name, name, new AttributesImpl());
        contentHandler.characters(s.toCharArray(), 0, s.length());
        contentHandler.endElement(namespaceUri(), name, name);
    }
    public SAXSource getSAXSource(){
        return new SAXSource(new Reader(this), new InputSource());
    }
    public void write(OutputStream os) throws TransformerConfigurationException, TransformerException{
        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        t.transform(getSAXSource(), new StreamResult(os));
    }
    
    private static final Attributes EMPTY_ATTRIBUTES = new AttributesImpl();
    //TODO: Deal with timezones
    public static Date readXsdDateTime(String s) throws ParseException{
        if(s.contains("Z")) 
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(s);
        else
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz").parse(s.replaceFirst(":(?=[0-9]{2}$)", ""));
    }
    public static String createXsdDateTime(Date d){
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(d);
    }
    public static Date readXsdDate(String s) throws ParseException{
        if(s.contains("Z")) 
            return new SimpleDateFormat("yyyy-MM-dd'Z'").parse(s);
        else
            return new SimpleDateFormat("yyyy-MM-ddz").parse(s.replaceFirst(":(?=[0-9]{2}$)", ""));
    }
    public static String createXsdDate(Date d){
        return new SimpleDateFormat("yyyy-MM-dd'Z'").format(d);
    }
}
