/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.awt.Color;
import java.io.Serializable;
import java.util.UUID;
import java.util.HashMap;
import java.util.Map;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.parameters.Changable;
import uk.destor.topos.data.parameters.Parameter;

/**
 * 
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoEntry extends XmlParser implements Serializable, Changable, Unique{   
    final Parameter<String> name = new Parameter<>(""), description = new Parameter<>(""), firstAscensionist = new Parameter<>(""), yearOfFirstAscent = new Parameter<>(""), grade = new Parameter<>("");
    private String uuid = null;
    final Parameter<Boolean> project = new Parameter<>(false, false);
    private final Map<String, TopoLine> topoLines = new HashMap<>();
    private TopoLine readingTopoLine = null;
    
    public TopoEntry() {
        super("Route");
    }

    @Override
    protected void elementRoot(String uri, String localName, String qName, Attributes attributes) {
        setName(attributes.getValue("name"));
        setGrade(attributes.getValue("grade"));
        setProject(Boolean.valueOf(attributes.getValue("project")));
        project.setInitial();
        project.setLast();
        setFirstAscensionist(attributes.getValue("fa"));
        setYearOfFirstAscent(attributes.getValue("faYear"));
        
        uuid = attributes.getValue("uuid");
    }
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "name", "name", null, getName());
        ai.addAttribute(namespaceUri(), "grade", "grade", null, getGrade());
        ai.addAttribute(namespaceUri(), "project", "project", null, Boolean.toString(isProject()));
        ai.addAttribute(namespaceUri(), "fa", "fa", null, getFirstAscensionist());
        ai.addAttribute(namespaceUri(), "faYear", "faYear", null, getYearOfFirstAscent());
        
        ai.addAttribute(namespaceUri(), "uuid", "uuid", null, getUuid());
        
        return ai;
    }

    @Override
    protected XmlParser startXmlElement(String uri, String localName, String qName, Attributes attributes) {
        if("TopoLine".equals(qName)) return readingTopoLine = new TopoLine();
        return null;
    }
    @Override
    public void endXmlElement(String uri, String localName, String qName){
        if("Description".equals(qName))
            setDescription(builder.toString().trim());
        if("TopoLine".equals(qName))
            topoLines.put(readingTopoLine.getTopoImage(), readingTopoLine);
    }
    @Override
    protected void outputChildren(ContentHandler contentHandler) throws SAXException {
        if(description != null)
            createStringElement(contentHandler, "Description", getDescription());
        for (TopoLine tl : topoLines.values())
            if(tl != null && tl.hasImage() && !tl.isEmpty())
                tl.createXmlElement(contentHandler);
    }
    
    private String emptyNotNull(Parameter<String> s){
        return s.getValue() == null ? "" : s.getValue();
    }
    public String getName() {
        return emptyNotNull(name);
    }
    public void setName(String name) {
        this.name.setValue(name);
    }

    public String getDescription() {
        return emptyNotNull(description);
    }
    public void setDescription(String description) {
        this.description.setValue(description);
    }
    
    public String getFirstAscensionist() {
        return emptyNotNull(firstAscensionist);
    }
    public void setFirstAscensionist(String firstAscensionist) {
        this.firstAscensionist.setValue(firstAscensionist);
    }
    
    public String getYearOfFirstAscent() {
        return emptyNotNull(yearOfFirstAscent);
    }
    public void setYearOfFirstAscent(String yearOfFirstAscent) {
        this.yearOfFirstAscent.setValue(yearOfFirstAscent);
    }

    public boolean isProject() {
        return project.getValue();
    }
    public void setProject(boolean project) {
        this.project.setValue(project);
    }

    public String getGrade() {
        return emptyNotNull(grade);
    }
    public void setGrade(String grade) {
        this.grade.setValue(grade);
    }

    public Map<String, TopoLine> getTopoLines() {
        return topoLines;
    }
    public TopoLine getTopoLine(String image){
        if(!topoLines.containsKey(image)) return null;
        return topoLines.get(image);
    }
    public void addTopoLine(TopoLine topoLine) {
        if(topoLine == null) return;
        
        topoLine.setColour(isProject() ? Color.YELLOW : Color.RED);
        topoLines.put(topoLine.getTopoImage(), topoLine);
    }

    @Override
    public String getUuid() {
        if(uuid == null)
            uuid = UUID.randomUUID().toString();
        return uuid;
    }
    
    /**
     * Returns the name for use in JLists.
     * @return 
     */
    @Override
    public String toString() {
        return getName();
    }

    @Override
    public void setLast() {
        name.setLast();
        description.setLast();
        firstAscensionist.setLast();
        yearOfFirstAscent.setLast();
        grade.setLast();
        project.setLast();
        
        for (TopoLine topoLine : topoLines.values()) {
            if(topoLine != null)
                topoLine.setLast();
        }
    }
    @Override
    public boolean hasChangedFromLast() {
        boolean changed = name.hasChangedFromLast() || description.hasChangedFromLast() || firstAscensionist.hasChangedFromLast() || 
                yearOfFirstAscent.hasChangedFromLast() || grade.hasChangedFromLast() || project.hasChangedFromLast();
        for (TopoLine topoLine : topoLines.values())
            changed |= (topoLine != null && topoLine.hasChangedFromLast());
        return changed;
    }
    @Override
    public void revertLast() {
        name.revertLast();
        description.revertLast();
        firstAscensionist.revertLast();
        yearOfFirstAscent.revertLast();
        grade.revertLast();
        project.revertLast();
        
        for (TopoLine topoLine : topoLines.values()) {
            if(topoLine != null)
                topoLine.revertLast();
        }
    }

    @Override
    public void setInitial() {
        name.setInitial();
        description.setInitial();
        firstAscensionist.setInitial();
        yearOfFirstAscent.setInitial();
        grade.setInitial();
        project.setInitial();
        
        for (TopoLine topoLine : topoLines.values()) {
            if(topoLine != null)
                topoLine.setInitial();
        }
    }
    @Override
    public boolean hasChangedFromInitial() {
        boolean changed =  name.hasChangedFromInitial() || description.hasChangedFromInitial() || firstAscensionist.hasChangedFromInitial() || 
                yearOfFirstAscent.hasChangedFromInitial() || grade.hasChangedFromInitial() || project.hasChangedFromInitial();
        for (TopoLine topoLine : topoLines.values())
                changed |= (topoLine != null && topoLine.hasChangedFromInitial());
        return changed;
    }

    int number = 0;
    public void setNumber(int n){
        number = n;
    }
    public String getNumber() {
        return String.valueOf(number);
    }
}
