/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerException;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.parameters.Changable;
import uk.destor.topos.data.parameters.Parameter;
import uk.destor.topos.output.Output;
import uk.destor.topos.ui.Main;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class Crag extends XmlParser implements Changable{
    final Parameter<String> name = new Parameter<>(""), topoAuthors = new Parameter<>(""), license = new Parameter<>("");
    private final List<TextArea> textAreas = new ArrayList<>();
    private final List<Area> areas = new ArrayList<>();
    private int textHash, areasHash;
    
    public Crag() {
        super("Crag");
        textHash = textAreas.hashCode();
        areasHash = areas.hashCode();
    }

    public Crag(File file) throws IOException{
        super("Crag");
        
        try(ZipFile zipFile = new ZipFile(file)){
            ZipEntry cragEntry = zipFile.getEntry("crag.xml");
            SAXParserFactory.newInstance().newSAXParser().parse(zipFile.getInputStream(cragEntry), this);
            
            for (Area a : getAreas()){
                a.setInitial();
                for (TopoImage ti : a.getTopoImages()){
                    InputStream is = zipFile.getInputStream(new ZipEntry("Areas/" + a.getName() + "/" + ti.getImageName()));
                    if(is != null) ti.setImage(ImageIO.read(is));
                }
            }
            
            for (TextArea ta : getTextAreas())
                if(ta.hasImage())
                    ta.setImage(ImageIO.read(zipFile.getInputStream(new ZipEntry("TextAreas/" + ta.getName() + "/" + ta.getImageName()))));
            
            setInitial();
            setLast();
        }catch(ParserConfigurationException | SAXException ex){
            throw new IOException("Error reading file.", ex);
        }
    }

    public void setName(String name) {
        this.name.setValue(name);
    }
    public String getName() {
        return name.getValue();
    }
    public Parameter<String> getLicenseParameter(){
        return license;
    }
    public void setLicense(String license){
        this.license.setValue(license);
    }
    public String getLicense(){
        return license.getValue();
    }
    public Parameter<String> getNameParameter(){
        return name;
    }
    public void setTopoAuthors(String topoAuthors) {
        this.topoAuthors.setValue(topoAuthors);
    }
    public String getTopoAuthors() {
        return topoAuthors.getValue();
    }
    public Parameter<String> getTopoAuthorsParameter(){
        return topoAuthors;
    }

    public List<TextArea> getTextAreas() {
        return textAreas;
    }
    public List<Area> getAreas() {
        return areas;
    }

    @Override
    protected void elementRoot(String uri, String localName, String qName, Attributes attributes) {
        setName(attributes.getValue("name"));
        setTopoAuthors(attributes.getValue("topoAuthors"));
    }
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "name", "name", null, getName());
        ai.addAttribute(namespaceUri(), "topoAuthors", "topoAuthors", null, getTopoAuthors());
        return ai;
    }

    @Override
    protected XmlParser startXmlElement(String uri, String localName, String qName, Attributes attributes) {
        if("Area".equals(qName)) return addToCollection(areas, new Area());
        if("Text".equals(qName)) return addToCollection(textAreas, new TextArea());
        return null;
    }
    @Override
    protected void endXmlElement(String uri, String localName, String qName) {
        if("License".equals(qName))
            license.setValue(builder.toString().trim());
    }
    
    @Override
    protected void outputChildren(ContentHandler contentHandler) throws SAXException {
        createXmlElements(contentHandler, areas);
        createXmlElements(contentHandler, textAreas);
        if(license.getValue() != null && !license.getValue().trim().isEmpty())
            createStringElement(contentHandler, "License", license.getValue());
    }
    
    public void writeToZip(OutputStream os) throws IOException{
        try (ZipOutputStream zos = new ZipOutputStream(os)) {
            zos.putNextEntry(new ZipEntry("crag.xml"));
            try {
                write(zos);
            } catch (TransformerException ex) {
                Logger.getLogger(Crag.class.getName()).log(Level.SEVERE, null, ex);
            }
            zos.closeEntry();
            
            for (TextArea ta : getTextAreas())
                if(ta.hasImage()){        
                    zos.putNextEntry(new ZipEntry("TextAreas/" + ta.getName() + "/" + ta.getImageName()));
                    try {
                        ImageIO.write((RenderedImage) ta.getImage(), ta.getImageName().substring(ta.getImageName().lastIndexOf(".") + 1), zos);
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    zos.closeEntry();
                }
            
            for (Area a : getAreas())
                for (TopoImage ti : a.getTopoImages()){
                    zos.putNextEntry(new ZipEntry("Areas/" + a.getName() + "/" + ti.getImageName()));
                    try {
                        ImageIO.write((RenderedImage) ti.getImage(), ti.getImageName().substring(ti.getImageName().lastIndexOf(".") + 1), zos);
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    zos.closeEntry();
                }
        }
    }
    public void createOutputFolder(File dir, Output output) throws IOException{
        if(dir.isFile()) throw new IllegalArgumentException("dir is a file, not a directory.");
        
        for (TextArea ta : getTextAreas())
            if(ta.hasImage()){        
                File f = new File(dir.toString(), "TextAreas/" + ta.getName() + "/" + ta.getImageName());
                if(!f.getParentFile().exists())
                    f.getParentFile().mkdirs();
                try {
                    saveImage(output, ta.getImage(), ta.getImageName(), f);
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        for (Area a : getAreas())
            for (TopoImage ti : a.getTopoImages()){
                File f = new File(dir.toString(), "Areas/" + a.getName() + "/" + ti.getImageName());
                if(!f.getParentFile().exists())
                    f.getParentFile().mkdirs();
                try {
                    saveImage(output, ti.getImage(), ti.getImageName(), f);
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }

    private void saveImage(Output output, Image i, String name, File f) throws IOException {
        String s = name.trim();
        ImageIO.write((RenderedImage) (output == null ? i : output.processImage((BufferedImage) i)), s.substring(s.lastIndexOf(".") + 1), f);
    }
    
    @Override
    public void setInitial() {
        textHash = textAreas.hashCode();
        areasHash = areas.hashCode();
        
        for (TextArea ta : textAreas)
            ta.setInitial();
        for (Area a : areas)
            a.setInitial();
        
        name.setInitial();
        topoAuthors.setInitial();
        license.setInitial();
    }

    @Override
    public boolean hasChangedFromInitial() {
        if(textHash != textAreas.hashCode() || areasHash != areas.hashCode())
            return true;
        
        for (TextArea ta : textAreas)
            if(ta.hasChangedFromInitial())
                return true;
        for (Area a : areas)
            if(a.hasChangedFromInitial())
                return true;
        
        return name.hasChangedFromInitial() || topoAuthors.hasChangedFromInitial() || license.hasChangedFromInitial();
    }

    @Override
    public void setLast() {
        for (TextArea ta : textAreas)
            ta.setLast();
        for (Area a : areas)
            a.setLast();
        
        name.setLast();
        topoAuthors.setLast();
        license.setLast();
    }

    @Override
    public boolean hasChangedFromLast() {
        for (TextArea ta : textAreas)
            if(ta.hasChangedFromLast())
                return true;
        for (Area a : areas)
            if(a.hasChangedFromLast())
                return true;
        
        return name.hasChangedFromLast() || topoAuthors.hasChangedFromLast() || license.hasChangedFromLast();
    }

    @Override
    public void revertLast() {
        for (TextArea ta : textAreas)
            ta.revertLast();
        for (Area a : areas)
            a.revertLast();
        
        name.revertLast();
        topoAuthors.revertLast();
        license.revertLast();
    }
}
