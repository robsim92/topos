/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public abstract class Diff {
    private final Diff parent;
    private final String name;
    private final List<DiffListItem> childern = new ArrayList<>();
    private final List<DiffParameter> parameters = new ArrayList<>();
    private boolean accepted = false, rejected = false;
    
    public Diff(Diff parent, String name) {
        this.parent = parent;
        this.name = name;
    }
    public Diff(String name) {
        this.parent = null;
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public Diff getParent() {
        return parent;
    }
    public List<DiffListItem> getChildern() {
        return childern;
    }
    public List<DiffParameter> getParameters() {
        return parameters;
    }

    public void reset(){
        rejected = accepted = false;
    }
    public void reject(){
        rejected = true;
    };
    public final void accept(){
        if(!accepted && !rejected){
            acceptImpl();
            accepted = true;
        }
    }

    public boolean isRejected(){
        return rejected;
    }
    public boolean isAccepted(){
        return accepted;
    }
    
    protected abstract void acceptImpl();
    protected void acceptChildern(){
        // This makes sure the moved items are updated first, so added items are in the right place
        for(DiffListItem d : getChildern())
            if(d.getStatus() == DiffListItem.Status.MOVED)
                d.accept();
        for(DiffListItem d : getChildern())
            if(d.getStatus() != DiffListItem.Status.MOVED)
                d.accept();
    }
    protected void acceptParameters(){
        for(DiffParameter dp : getParameters())
            dp.accept();
    }
}
