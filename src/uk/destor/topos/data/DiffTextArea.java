/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class DiffTextArea extends DiffListItem<TextArea>{
    public static Creator<TextArea> CREATOR = new Creator<TextArea>() {
        @Override
        public DiffListItem<TextArea> createNew(Diff parent, List<TextArea> list, List<String> previousUuids, TextArea item, Map<String, TextArea> uuidMap) {
            return new DiffTextArea(parent, list, previousUuids, item, uuidMap, true);
        }
        @Override
        public DiffListItem<TextArea> createMoved(Diff parent, List<TextArea> list, List<String> previousUuids, TextArea item, Map<String, TextArea> uuidMap) {
            return new DiffTextArea(parent, list, previousUuids, item, uuidMap, false);
        }
        @Override
        public DiffListItem<TextArea> createExisting(Diff parent, TextArea item1, TextArea item2) {
            return new DiffTextArea(parent, item1, item2);
        }
        @Override
        public DiffListItem<TextArea> createDelete(Diff parent, List<TextArea> list, TextArea item) {
            return new DiffTextArea(parent, list, item);
        }
    };

    private final DiffParameter<String> name, text, imageName, imageCaption;
    private final DiffParameter<Boolean> showTitle;
    
    public DiffTextArea(Diff parent, List<TextArea> list, List<String> previousUuids, TextArea item, Map<String, TextArea> uuidMap, boolean isNew) {
        super(parent, "Text Area (" + item.getName() + ")", list, previousUuids, item, uuidMap, isNew);
        name = text = imageName = imageCaption = null;
        showTitle = null;
    }
    public DiffTextArea(Diff parent, List<TextArea> list, TextArea ta) {
        super(parent, "Text Area (" + ta.getName() + ")", list, ta);
        name = text = imageName = imageCaption = null;
        showTitle = null;
    }
    public DiffTextArea(Diff parent, TextArea ta1, TextArea ta2) {
        super(parent, "Text Area (" + ta1.getName() + ")");
        
        getParameters().add(name = new DiffParameter<>(this, "Name", ta1.name, ta2.getName()));
        getParameters().add(text = new DiffParameter<>(this, "Text", ta1.text, ta2.getText()));
        getParameters().add(showTitle = new DiffParameter<>(this, "Show Title", ta1.showTitle, ta2.showTitle()));
        getParameters().add(imageName = new DiffParameter<>(this, "Image Name", ta1.imageName, ta2.getImageName()));
        getParameters().add(imageCaption = new DiffParameter<>(this, "Image Caption", ta1.caption, ta2.getCaption()));
    }

    public String getName(boolean original){
        if(name == null){
            if(getStatus().equals(Status.NEW))
                return original?"":item.getName();
            else
                return original?item.getName():"";
        }else
            return original?name.getParameter().getValue():name.getValue();
    }
    public boolean showTitle(boolean original){
        return original?showTitle.getParameter().getValue():showTitle.getValue();
    }
    public String getText(boolean original){
        return original?text.getParameter().getValue():text.getValue();
    }
    public String getImageName(boolean original){
        return original?imageName.getParameter().getValue():imageName.getValue();
    }
    public String getImageCaption(boolean original){
        return original?imageCaption.getParameter().getValue():imageCaption.getValue();
    }

    public DiffParameter<String> getNameParameter(){
        return name;
    }
    public DiffParameter<String> getText(){
        return text;
    }
    public DiffParameter<String> getImageName(){
        return imageName;
    }
    public DiffParameter<String> getImageCaption(){
        return imageCaption;
    }
    public DiffParameter<Boolean> getShowTitle(){
        return showTitle;
    }
}
