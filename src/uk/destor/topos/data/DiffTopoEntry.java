/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class DiffTopoEntry extends DiffListItem<TopoEntry>{
    private final DiffParameter<String> name, grade, fa, faYear, description;
    private final DiffParameter<Boolean> project;
    
    public static Creator<TopoEntry> CREATOR = new Creator<TopoEntry>() {
        @Override
        public DiffListItem<TopoEntry> createNew(Diff parent, List<TopoEntry> list, List<String> previousUuids, TopoEntry item, Map<String, TopoEntry> uuidMap) {
            return new DiffTopoEntry(parent, list, previousUuids, item, uuidMap, true);
        }
        @Override
        public DiffListItem<TopoEntry> createMoved(Diff parent, List<TopoEntry> list, List<String> previousUuids, TopoEntry item, Map<String, TopoEntry> uuidMap) {
            return new DiffTopoEntry(parent, list, previousUuids, item, uuidMap, false);
        }
        @Override
        public DiffListItem<TopoEntry> createExisting(Diff parent, TopoEntry item1, TopoEntry item2) {
            return new DiffTopoEntry(parent, item1, item2);
        }
        @Override
        public DiffListItem<TopoEntry> createDelete(Diff parent, List<TopoEntry> list, TopoEntry item) {
            return new DiffTopoEntry(parent, list, item);
        }
    };
    
    private final List<DiffTopoLine> topoLines = new ArrayList<>();

    public DiffTopoEntry(Diff parent, List<TopoEntry> list, List<String> previousUuids, TopoEntry item, Map<String, TopoEntry> uuidMap, boolean isNew) {
        super(parent, "TopoEntry (" + item.getName() + ")", list, previousUuids, item, uuidMap, isNew);
        name = grade = fa = faYear = description = null;
        project = null;
        if(isNew)
            for(TopoLine tl : item.getTopoLines().values())
                topoLines.add(new DiffTopoLine(null, tl, item));
    }
    public DiffTopoEntry(Diff parent, List<TopoEntry> list, TopoEntry te) {
        super(parent, "TopoEntry (" + te.getName() + ")", list, te);
        name = grade = fa = faYear = description = null;
        project = null;
        for(TopoLine tl : item.getTopoLines().values())
            topoLines.add(new DiffTopoLine(tl, null, item));
    }
    public DiffTopoEntry(Diff parent, TopoEntry te1, TopoEntry te2) {
        super(parent, "TopoEntry (" + te1.getName() + ")");
        
        getParameters().add(name = new DiffParameter<>(this, "Name", te1.name, te2.getName()));
        getParameters().add(grade = new DiffParameter<>(this, "Grade", te1.grade, te2.getGrade()));
        getParameters().add(fa = new DiffParameter<>(this, "First Ascensionist", te1.firstAscensionist, te2.getFirstAscensionist()));
        getParameters().add(faYear = new DiffParameter<>(this, "Year of First Axcent", te1.yearOfFirstAscent, te2.getYearOfFirstAscent()));
        getParameters().add(project = new DiffParameter<>(this, "Project", te1.project, te2.isProject()));
        getParameters().add(description = new DiffParameter<>(this, "Description", te1.description, te2.getDescription()));
        
        Map<String, TopoLine> map1 = new HashMap<>(), map2 = new HashMap<>();
        for(TopoLine e : te2.getTopoLines().values())
            map2.put(e.getUuid(), e);
        
        for(TopoLine e : te1.getTopoLines().values()){
            map1.put(e.getUuid(), e);
            
            if(map2.containsKey(e.getUuid()))
                topoLines.add(new DiffTopoLine(e, te2.getTopoLine(e.getTopoImage()), te1));
            else
                topoLines.add(new DiffTopoLine(e, null, te1));
        }
        
        for(TopoLine e : te2.getTopoLines().values()){
            if(!map1.containsKey(e.getUuid()))
                topoLines.add(new DiffTopoLine(null, e, te1));
        }
    }
    private void createTopoLines(TopoEntry te){
    }
    
    @Override
    public boolean hasChanged() {
        if(super.hasChanged()) return true;
        
        for (DiffTopoLine tl : topoLines)
            if(!tl.getStatus().equals(DiffTopoLine.Status.EXISTING_SAME))
                return true;
        
        return false;
    }
    @Override
    protected void acceptChanges() {
        for (DiffTopoLine tl : topoLines)
            tl.accept();
    }

    public String getName(boolean original){
        return original?name.getParameter().getValue():name.getValue();
    }
    public String getGrade(boolean original){
        return original?grade.getParameter().getValue():grade.getValue();
    }
    public String getFa(boolean original){
        return original?fa.getParameter().getValue():fa.getValue();
    }
    public String getFaYear(boolean original){
        return original?faYear.getParameter().getValue():faYear.getValue();
    }
    public String getDescription(boolean original){
        return original?description.getParameter().getValue():description.getValue();
    }
    public boolean getProject(boolean original){
        return original?project.getParameter().getValue():project.getValue();
    }
    public List<DiffTopoLine> getTopoLines(){
        return topoLines;
    }
    
    public boolean hasNameChanged(){
        return name.hasChanged();
    }
    public boolean hasGradeChanged(){
        return grade.hasChanged();
    }
    public boolean hasFaChanged(){
        return fa.hasChanged();
    }
    public boolean hasFaYearChanged(){
        return faYear.hasChanged();
    }
    public boolean hasDescriptionChanged(){
        return description.hasChanged();
    }
    public boolean hasProjectChanged(){
        return project.hasChanged();
    }
    public boolean hasTopoLinesChanged(){
        for(DiffTopoLine tl : topoLines)
            if(!tl.getStatus().equals(DiffTopoLine.Status.EXISTING_SAME))
                return true;
        return false;
    }

    public DiffParameter<String> getNameParameter(){
        return name;
    }
    public DiffParameter<String> getGrade(){
        return grade;
    }
    public DiffParameter<String> getFa(){
        return fa;
    }
    public DiffParameter<String> getFaYear(){
        return faYear;
    }
    public DiffParameter<String> getDescription(){
        return description;
    }
    public DiffParameter<Boolean> getProject(){
        return project;
    }
}
