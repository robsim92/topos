/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class DiffArea extends DiffListItem<Area>{
    public static Creator<Area> CREATOR = new Creator<Area>() {
        @Override
        public DiffListItem<Area> createNew(Diff parent, List<Area> list, List<String> previousUuids, Area item, Map<String, Area> uuidMap) {
            return new DiffArea(parent, list, previousUuids, item, uuidMap, true);
        }
        @Override
        public DiffListItem<Area> createMoved(Diff parent, List<Area> list, List<String> previousUuids, Area item, Map<String, Area> uuidMap) {
            return new DiffArea(parent, list, previousUuids, item, uuidMap, false);
        }
        @Override
        public DiffListItem<Area> createExisting(Diff parent, Area item1, Area item2) {
            return new DiffArea(parent, item1, item2);
        }
        @Override
        public DiffListItem<Area> createDelete(Diff parent, List<Area> list, Area item) {
            return new DiffArea(parent, list, item);
        }
    };
    
    private final List<DiffTopoEntry> diffTopoEntrys = new ArrayList<>();
    private final Map<String, List<DiffTopoLine>> topoLines = new HashMap<>();
    private final Area area1, area2;
    private final DiffParameter<String> name, description;
    
    public DiffArea(Diff parent, List<Area> list, List<String> previousUuids, Area item, Map<String, Area> uuidMap, boolean isNew) {
        super(parent, "Area (" + item.getName() + ")", list, previousUuids, item, uuidMap, isNew);
        if(isNew){
            area1 = null;
            area2 = item;
        }else{
            area1 = item;
            area2 = null;
        }
        name = description = null;
        populateTopos(item);
    }
    public DiffArea(Diff parent, List<Area> list, Area ta) {
        super(parent, "Area (" + ta.getName() + ")", list, ta);
        area1 = ta;
        area2 = null;
        name = description = null;
        populateTopos(item);
    }
    public DiffArea(Diff parent, Area a1, Area a2) {
        super(parent, "Area (" + a1.getName() + ")");
        
        getParameters().add(name = new DiffParameter<>(this, "Name", a1.name, a2.getName()));
        getParameters().add(description = new DiffParameter<>(this, "Description", a1.description, a2.getDescription()));
        
        
        List<DiffListItem<TopoEntry>> items = DiffListItem.listDiff(DiffTopoEntry.CREATOR, this, a1.getTopoEntries(), a2.getTopoEntries());
        getChildern().addAll(items);
        for(DiffListItem<TopoEntry> item : items){
            DiffTopoEntry dte = (DiffTopoEntry) item;
            diffTopoEntrys.add(dte);
            if(dte.getStatus().equals(DiffListItem.Status.EXISTING) || dte.getStatus().equals(DiffListItem.Status.MOVED))
                for(DiffTopoLine dtl : dte.getTopoLines()){
                    if(dtl.getStatus().equals(DiffTopoLine.Status.EXISTING_CHANGED)){
                        if(!topoLines.containsKey(dtl.getTopoLine2().getTopoImage()))
                            topoLines.put(dtl.getTopoLine2().getTopoImage(), new ArrayList<>());
                        topoLines.get(dtl.getTopoLine2().getTopoImage()).add(dtl);
                    }
                }
        }
        
        area1 = a1;
        area2 = a2;
    }
    private void populateTopos(Area a){        
        Map<String, TopoEntry> map = new HashMap<>();
        List<String> uuids = new LinkedList<>();

        for(TopoEntry te : a.getTopoEntries()){
            diffTopoEntrys.add(new DiffTopoEntry(this, a.getTopoEntries(), new ArrayList<String>(uuids), te, map, true));
            uuids.add(te.getUuid());
        }
        getChildern().addAll(diffTopoEntrys);
    }

    public Area getArea1(){
        return area1;
    }
    public Area getArea2(){
        return area2;
    }

    @Override
    protected void acceptChanges(){
        for(DiffTopoEntry dte : diffTopoEntrys)
            dte.accept();
    }
    
    public List<DiffTopoEntry> getDiffTopoEntrys(){
        return diffTopoEntrys;
    }
    public Map<String, List<DiffTopoLine>> getTopoLines(){
        return topoLines;
    }
    
    public String getName(boolean original){
        return original?name.getParameter().getValue():name.getValue();
    }
    public String getDescription(boolean original){
        return original?description.getParameter().getValue():description.getValue();
    }

    public DiffParameter<String> getNameParameter(){
        return name;
    }

    public DiffParameter<String>  getDescription(){
        return description;
    }
}
