/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public abstract class DiffListItem<E extends Unique> extends Diff{
    public interface Creator<E extends  Unique>{
        public DiffListItem<E> createNew(Diff parent, List<E> list, List<String> previousUuids, E item, Map<String, E> uuidMap);
        public DiffListItem<E> createExisting(Diff parent, E item1, E item2);
        public DiffListItem<E> createMoved(Diff parent, List<E> list, List<String> previousUuids, E item, Map<String, E> uuidMap);
        public DiffListItem<E> createDelete(Diff parent, List<E> list, E item);
    }
    public enum Status{
        NEW{
            @Override
            public void accept(DiffListItem item) {
                item.uuidMap.put(item.item.getUuid(), item.item);
                String uuid = null;
                for(ListIterator<String> it = item.previousUuids.listIterator(item.previousUuids.size()); it.hasPrevious();){
                    if(item.uuidMap.containsKey(uuid = it.previous())){
                        item.list.add(item.list.indexOf(item.uuidMap.get(uuid)) + 1, item.item);
                        return;
                    }
                }
                item.list.add(0, item.item);
            }
        },
        EXISTING {
            @Override
            public void accept(DiffListItem item) {
                item.acceptParameters();
                item.acceptChanges();
            }
        },
        MOVED {
            @Override
            public void accept(DiffListItem item) {
                DELETED.accept(item);
                NEW.accept(item);
            }  
        },
        DELETED {
            @Override
            public void accept(DiffListItem item) {
                item.list.remove(item.item);
            }
        };
        public abstract void accept(DiffListItem item);
    }
    private final List<E> list;
    private final List <String> previousUuids;
    protected final E item;
    private final Map<String, E> uuidMap;
    private final Status status;

    public DiffListItem(Diff parent, String name, List<E> list, List<String> previousUuids, E item, Map<String, E> uuidMap, boolean isNew) {
        super(parent, name);
        this.list = list;
        this.previousUuids = previousUuids;
        this.item = item;
        this.uuidMap = uuidMap;
        status = isNew ? Status.NEW : Status.MOVED;
    }
    public DiffListItem(Diff parent, String name) {
        super(parent, name);
        this.list = null;
        this.previousUuids = null;
        this.item = null;
        this.uuidMap = null;
        status = Status.EXISTING;
    }
    public DiffListItem(Diff parent, String name, List<E> list, E item) {
        super(parent, name);
        this.list = list;
        this.previousUuids = null;
        this.item = item;
        this.uuidMap = null;
        status = Status.DELETED;
    }
    
    public boolean hasChanged(){
        if(status != Status.EXISTING)
            return true;
        
        for (DiffParameter p : getParameters())
            if(p.hasChanged())
                return true;
        
        for (DiffListItem di : getChildern())
            if(di.hasChanged())
                return true;
        
        return false;
    }
    @Override
    protected void acceptImpl() {
        status.accept(this);
    }
    protected void acceptChanges(){}
    public Status getStatus() {
        return status;
    }
    public E getItem(){
        return item;
    }
    
    public static <E extends Unique> List<DiffListItem<E>> listDiff(Creator<E> c, Diff parent, List<E> l1, List<E> l2){
        List<DiffListItem<E>> items = new ArrayList<>();
        
        Map<String, E> map1 = new HashMap<>(), map2 = new HashMap<>();
        List<String> uuids1 = new LinkedList<>();
        
        for(E e : l2)
            map2.put(e.getUuid(), e);
        
        for(E e : l1){
            map1.put(e.getUuid(), e);
            uuids1.add(e.getUuid());
            
            if(map2.containsKey(e.getUuid()))                
                items.add(c.createExisting(parent, e, map2.get(e.getUuid())));
            else
                items.add(c.createDelete(parent, l1, e));
        }
        
        List<String> uuids2 = new ArrayList<>();
        for(E e : l2){
            if(!map1.containsKey(e.getUuid()))
                items.add(c.createNew(parent, l1, new ArrayList<>(uuids2), e, map1));
            uuids2.add(e.getUuid());
        }
        
        // Find moved items
        //   Remove new items from uuid2
        for (Iterator<String> it = uuids2.iterator(); it.hasNext();) {
            String u = it.next();
            if(!uuids1.contains(u))
                it.remove();
        }
        
        //   Remove deleted items from uuid1
        for (Iterator<String> it = uuids1.iterator(); it.hasNext();) {
            String u = it.next();
            if(!uuids2.contains(u))
                it.remove();
        }
        
        //   We also need to remove moved items from the lists to stop double counting
        for(int q = 0; q < uuids1.size();){
            if(uuids2.indexOf(uuids1.get(q)) != q){
                // Moved
                List<String> uuids = new ArrayList<>();
                for(E e : l2){
                    if(e.getUuid().equals(uuids1.get(q)))
                        break;
                    uuids.add(e.getUuid());
                }
                items.add(c.createMoved(parent, l1, uuids, map1.get(uuids1.get(q)), map1));
                uuids2.remove(uuids1.get(q));
                uuids1.remove(q);
            }else
                q++;
        }
        
        return items;
    }
}
