/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.parameters.Changable;
import uk.destor.topos.data.parameters.Parameter;

// TODO: Separate Data and Graphics
/**
 * See https://pomax.github.io/bezierinfo for the maths.
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoLine extends XmlParser implements Serializable, Changable, Unique{

    private enum DragPoint{
        POSITION{
            @Override
            public void move(LinePoint lp, Point p) {
                lp.movePosition(p);
            }
        }, CONTROL_1{
            @Override
            public void move(LinePoint lp, Point p) {
                lp.moveControl1(p);
            }
        }, CONTROL_2{
            @Override
            public void move(LinePoint lp, Point p) {
                lp.moveControl2(p);
            }
        };
        public abstract void move(LinePoint lp,  Point p);
    }
    public interface TopoPoint{
        public Point2D.Double getControl1();
        public Point2D.Double getControl2();
        public Point2D.Double getPosition();
    }
    private class LinePoint implements TopoPoint{    
        private Point2D.Double position, control1, control2;
        private Point lookupPosition, lookupC1, lookupC2;
        private boolean mouseOverPosition = false, mouseOverC1 = false, mouseOverC2 = false;
        private LineSegment ls1, ls2;

        public LinePoint(String assymptote){
            String ss[] = assymptote.replace("controls", "").split("\\.\\.");
            if(assymptote.contains("controls") && ss.length == 2){
                position = parsePoint(ss[0]);
                control1 = null;
                control2 = parsePoint(ss[1]);
            }else if(ss.length == 2){
                position = parsePoint(ss[1]);
                control1 = parsePoint(ss[0]);
                control2 = null;
            }else{
                position = parsePoint(ss[1]);
                control1 = parsePoint(ss[0]);
                control2 = parsePoint(ss[2]);
            }
        }
        public LinePoint(Point2D.Double position, Point2D.Double control1, Point2D.Double control2) {
            this.position = position;
            this.control1 = control1;
            this.control2 = control2;
            updateLookup();
        }
        
        public void updateLookup(){
            lookupPosition = toLookup(position);
            lookupC1 = toLookup(control1);
            lookupC2 = toLookup(control2);
        }
        
        public void draw(Graphics2D g){
            drawLine(g, lookupPosition, lookupC1);
            drawLine(g, lookupPosition, lookupC2);
            drawOval(g, lookupPosition, mouseOverPosition);
            drawOval(g, lookupC1, mouseOverC1);
            drawOval(g, lookupC2, mouseOverC2);
        }
        
        public boolean isMouseOverPosition(Point p, int distance){
            return lookupPosition != null && (mouseOverPosition = lookupPosition.distanceSq(p) < distance);
        }
        public boolean isMouseOverPosition(Point p){
            return isMouseOverPosition(p, DD);
        }
        public boolean isMouseOverControl1(Point p){
            return lookupC1 != null && (mouseOverC1 = lookupC1.distanceSq(p) < DD);
        }
        public boolean isMouseOverControl2(Point p){
            return lookupC2 != null && (mouseOverC2 = lookupC2.distanceSq(p) < DD);
        }
        
        public Point2D.Double getPosition() {
            return position;
        }
        public void setPosition(Point2D.Double position) {
            this.position = position;
        }
        public void movePosition(Point newPosition){
            double dx = newPosition.x - lookupPosition.x, dy = newPosition.y - lookupPosition.y;
            lookupPosition.setLocation(newPosition);
            movePoint(lookupC1, dx, dy);
            movePoint(lookupC2, dx, dy);
            
            dx /= imageDimensions.x*scale; dy /= imageDimensions.y*scale;
            movePoint(position, dx, dy);
            movePoint(control1, dx, dy);
            movePoint(control2, dx, dy);
        }
        public void moveControl1(Point newPosition){
            lookupC1.setLocation(newPosition);
            control1 = toImage(lookupC1);
        }
        public void moveControl2(Point newPosition){
            lookupC2.setLocation(newPosition);
            control2 = toImage(lookupC2);
        }
        private void movePoint(Point2D p, double dx, double dy){
            if(p == null) return;
            p.setLocation(p.getX() + dx, p.getY() + dy);
        }
        
        public Point2D.Double getControl1() {
            return control1;
        }
        public void setControl1(Point2D.Double control1) {
            this.control1 = control1;
        }
        public Point2D.Double getControl2() {
            return control2;
        }
        public void setControl2(Point2D.Double control2) {
            this.control2 = control2;
        }

        public void setLineSegment1(LineSegment ls) {
            this.ls1 = ls;
        }
        public boolean hasLineSegment1(){
            return ls1 != null;
        }
        public LineSegment getLineSegment1() {
            return ls1;
        }
        public void setLineSegment2(LineSegment ls) {
            this.ls2 = ls;
        }
        public boolean hasLineSegment2(){
            return ls2 != null;
        }
        public LineSegment getLineSegment2(){
            return ls2;
        }
        
        public Point getLookupPosition() {
            return lookupPosition;
        }
        public Point getLookupC1() {
            return lookupC1;
        }
        public Point getLookupC2() {
            return lookupC2;
        }
        
        public String assymptoteString(){
            return c1Assymptote() + pointToString(position) + c2Assymptote();
        }
        public String positionAssymptote(){
            return pointToString(position);
        }
        public String c1Assymptote(){
            return control1 == null ? "" :  pointToString(control1) + "..";
        }
        public String c2Assymptote(){
            return control2 == null ? "" : "..controls" + pointToString(control2) + "and";
        }
        private String pointToString(Point2D p){
            return "(" + String.valueOf(p.getX()) + "," + String.valueOf(1 - p.getY()) + ")";
        }
        private Point2D.Double parsePoint(String s){
            String ss[] = s.replace("(", "").replace(")", "").split(",");
            return new Point2D.Double(Double.parseDouble(ss[0]), 1 - Double.parseDouble(ss[1]));
        }
    }
    private class LineSegment{
        private LinePoint point1, point2;
        private int mouseOver = -1, start;

        @SuppressWarnings("LeakingThisInConstructor")
        public LineSegment(LinePoint point1, LinePoint point2) {
            this.point1 = point1;
            this.point2 = point2;
            point1.setLineSegment2(this);
            point2.setLineSegment1(this);
        }

        public void setPoint1(LinePoint point1) {
            this.point1 = point1;
            TopoLine.this.updateLookupTable();
            point1.setLineSegment2(this);
        }
        public LinePoint getPoint1() {
            return point1;
        }
        public void setPoint2(LinePoint point2) {
            this.point2 = point2;
            TopoLine.this.updateLookupTable();
            point2.setLineSegment1(this);
        }
        public LinePoint getPoint2() {
            return point2;
        }
        
        public boolean isMouseOver(Point p){
            mouseOver = getPointOnCurve(p);
            return mouseOver != -1;
        }
        
        public void updateLookupTable(int start){
            this.start = start;
            forPoint((Integer t, Point2D.Double u) -> {
                toLookup(u, lookupTableX, lookupTableY, start + t);
            });
        }
        public void forPoint(BiConsumer<Integer, Point2D.Double> bc){
            bc.accept(0, point1.position);
            
            double dt = 1d/(SECTION_SIZE + 1), t = dt;
            for(int q = 1; q < SECTION_SIZE; q++)
                bc.accept(q, getPoint(t += dt));
        }
        
        public LinePoint split(){
            double t = (double)mouseOver/(double)(SECTION_SIZE + 2), t1 = 1-t;
            
            Point2D.Double c1c2 = getMid(t, t1, point1.control2, point2.control1);
            point1.control2.setLocation(getMid(t, t1, point1.position, point1.control2));
            point2.control1.setLocation(getMid(t, t1, point2.control1, point2.position));
            Point2D.Double c1 = getMid(t, t1, point1.control2, c1c2);
            Point2D.Double c2 = getMid(t, t1, c1c2, point2.control1);
            
            LinePoint mid = new LinePoint(getMid(t, t1, c1, c2), c1, c2);
            mid.setLineSegment1(this);
            
            lineSegments.add(lineSegments.indexOf(this) + 1, new LineSegment(mid, point2));
            linePoints.add(linePoints.indexOf(point1) + 1, mid);
            
            point2 = mid;
            mouseOver = -1;
            
            return mid;
        }
        private Point2D.Double getMid(double t, double t1, Point2D.Double p1, Point2D.Double p2){
            return new Point2D.Double(t1*p1.x + t*p2.x, t1*p1.y + t*p2.y);
        }
        
        public int getPointOnCurve(Point p){
           if(parent == null) return -1;
            for(int q = 1; q < SECTION_SIZE - 1; q++)
                if(p.distanceSq(lookupTableX[start + q], lookupTableY[start + q]) < DD)
                    return q;
            return -1;
        }
        public Point2D.Double getPoint(double t) {
            double tt = t * t;
            double ttt = tt * t;
            double mt = 1 - t;
            double mtt = mt * mt;
            double mttt = mtt * mt;
            double t2 = 3 * mtt * t;
            double t3 = 3 * mt * tt;
            try {
                return new Point2D.Double(
                        point1.position.x * mttt + point1.control2.x * t2 + point2.control1.x * t3 + point2.position.x * ttt,
                        point1.position.y * mttt + point1.control2.y * t2 + point2.control1.y * t3 + point2.position.y * ttt);
            } catch (Exception e) {
                System.out.println("");
                return null;
            }
        }
        public void draw(Graphics2D g){
//            Point last = point1.lookupPosition;
//            for (Point p : lookupTable){
//                drawLine(g, last, p);
//                last = p;
//            }
//            drawLine(g, last, point2.lookupPosition);
        }
        public void drawMouse(Graphics2D g){
            if(mouseOver != -1)
                drawOval(g, lookupTableX, lookupTableY, mouseOver, true);
        }
    }
    private class CreateLine extends MouseAdapter{
        private Point.Double first = null, last = null;
        @Override
        public void mouseClicked(MouseEvent e) {
            if(e.getButton() == MouseEvent.BUTTON1){
                if(first == null)
                    first = toImage(e.getPoint());
                else if(last == null)
                    last = toImage(e.getPoint());
                if(first != null && last != null){
                    double dx = last.getX() - first.getX(), dy = last.getY() - first.getY();
                    double distance = first.distance(last);
                    if(distance > 50){
                        dx *= 25/distance;
                        dy *= 25/distance;
                    }else{
                        dx /= 3;
                        dy /= 3;
                    }
                    LinePoint p1 = new LinePoint(first, null, new Point2D.Double(first.x + dx, first.y + dy));
                    LinePoint p2 = new LinePoint(last, new Point2D.Double(last.x - dx, last.y - dy), null);
                    linePoints.add(p1);
                    linePoints.add(p2);
                    lineSegments.add(new LineSegment(p1, p2));
                    first = last = null;
                    removeMouseAdaptor(this);
                    addMouseAdaptor(editLine);
                }
                parent.repaint();
                e.consume();
            }else if(e.getButton() == MouseEvent.BUTTON3){
                first = last = null;
                parent.repaint();
                e.consume();
            }
        }

        public void setFirst(Point.Double first) {
            this.first = first;
            last = null;
        }
        public void setLast(Point.Double last) {
            this.last = last;
            first = null;
        }
        
        @Override
        public void mouseMoved(MouseEvent e) {
            parent.repaint();
        }
        public void draw(Graphics2D g){
            if(first != null){
                drawOval(g, toLookup(first), true);
                drawLine(g, toLookup(first), parent.getMousePosition());
            }else if(last != null){
                drawOval(g, toLookup(last), true);
                drawLine(g, toLookup(last), parent.getMousePosition());
            }
        }
    }
    private class EditLine extends MouseAdapter{
        private LinePoint dragLP = null;
        private LineSegment lineSegment = null;
        private DragPoint dragPoint;
        @Override
        public void mouseMoved(MouseEvent e) {
            boolean repaint = dragLP != null || lineSegment != null;
            dragLP = null;
            lineSegment = null;
            for(LinePoint lp : linePoints){
                if(lp.isMouseOverPosition(e.getPoint())){
                    dragLP = lp;
                    dragPoint = DragPoint.POSITION;
                    parent.repaint();
                    return;
                }else if(lp.isMouseOverControl1(e.getPoint())){
                    dragLP = lp;
                    dragPoint = DragPoint.CONTROL_1;
                    parent.repaint();
                    return;
                }else if(lp.isMouseOverControl2(e.getPoint())){
                    dragLP = lp;
                    dragPoint = DragPoint.CONTROL_2;
                    parent.repaint();
                    return;
                }else if(tag.isMouseOverPosition(e.getPoint(), 2*DD)){
                    dragLP = tag;
                    dragPoint = DragPoint.POSITION;
                    parent.repaint();
                    return;
                }
            }
            for(LineSegment ls : lineSegments)
                if(ls.isMouseOver(e.getPoint())){
                    lineSegment = ls;
                    parent.repaint();
                    return;
                }
            
            if(repaint)
                parent.repaint();
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if(e.getButton() == MouseEvent.BUTTON1 && lineSegment != null){
                dragLP = lineSegment.split();
                updateLookupTable();
                
                dragPoint = DragPoint.POSITION;
                lineSegment = null;
                
                parent.repaint();
                e.consume();
            }
        }
        
        @Override
        public void mouseDragged(MouseEvent e) {
            if(dragLP != null){
                dragPoint.move(dragLP, e.getPoint());
                TopoLine.this.updateLookupTable();
                parent.repaint();
                e.consume();
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if(e.getButton() == MouseEvent.BUTTON3 && dragLP != null && dragPoint == DragPoint.POSITION){
                LineSegment ls1 = dragLP.getLineSegment1(), ls2 = dragLP.getLineSegment2();
                if(ls1 == null){
                    ls2.getPoint2().setControl1(null);
                    ls2.getPoint2().setLineSegment1(null);
                    lineSegments.remove(ls2);
                }else if(ls2 == null){
                    ls1.getPoint1().setControl2(null);
                    ls2.getPoint2().setLineSegment2(null);
                    lineSegments.remove(ls1);
                }else{
                    ls1.setPoint2(ls2.getPoint2());
                    lineSegments.remove(ls2);
                }
                linePoints.remove(dragLP);
                if(!linePoints.isEmpty() && lineSegments.isEmpty()){
                    if(ls1 == null)
                        createLine.setFirst(linePoints.get(0).position);
                    else
                        createLine.setLast(linePoints.get(0).position);
                    linePoints.clear();
                    removeMouseAdaptor(this);
                    addMouseAdaptor(createLine);
                }
                dragLP = null;
                updateLookupTable();
                parent.repaint();
                e.consume();
            }
        }

        public DragPoint getDragPoint(){
            return dragPoint;
        }
        public LinePoint getDragLP(){
            return dragLP;
        }
    }
    
    private final static int OVAL_SIZE = 8, OVAL_SIZE_2 = OVAL_SIZE/2, DD = 25, SECTION_SIZE = 1000;
    private final List<LinePoint> linePoints = new ArrayList<>();
    private final List<LineSegment> lineSegments = new ArrayList<>();
    private  int lookupTableX[], lookupTableY[];
    
    private LinePoint tag = null;
    
    private final Parameter<String> topoImage = new Parameter<>(null);
    private final CreateLine createLine = new CreateLine();
    private final EditLine editLine = new EditLine();
    private boolean editable = false;
    private Component parent;
    private Color colour = Color.RED;
    
    private final Point imageDimensions = new Point(), offset = new Point();
    private double scale = 1;
    
    private String initalString, lastString;
    private Point2D.Double initialTag, lastTag;
    private String uuid = null;

    public TopoLine() {
        super("TopoLine");
    }
    public TopoLine(Component parent, Point imgSize, Point offset, double scale){
        this();
        setParent(parent, imgSize, offset, scale);
        setEditable(true);
    }
        
    private void addMouseAdaptor(MouseAdapter ma){
        parent.addMouseListener(ma);
        parent.addMouseMotionListener(ma);
    }
    private void removeMouseAdaptor(MouseAdapter ma){
        parent.removeMouseListener(ma);
        parent.removeMouseMotionListener(ma);
    }
    private Point2D.Double.Double toImage(Point p){
        if(p == null || parent == null) return null;
        return new Point2D.Double((double)(p.x - offset.x)/(double)imageDimensions.x/scale, (double)(p.y - offset.y)/(double)imageDimensions.y/scale);
    }
    private Point toLookup(Point2D.Double p){
        if(p == null || parent == null) return null;
        return new Point((int)(p.getX()*imageDimensions.x*scale + offset.x), (int) (p.getY()*imageDimensions.y*scale + offset.y));
    }
    private void toLookup(Point2D.Double p, int x[], int y[], int pos){
        if(p == null || parent == null) return;
        x[pos] = (int)(p.getX()*imageDimensions.x*scale + offset.x);
        y[pos] = (int)(p.getY()*imageDimensions.y*scale + offset.y);
    }
    public String assymptoteString(){
        return assymptoteString(true, true);
    }
    public String assymptoteString(boolean start, boolean end){
        if(linePoints.size() < 2) return "";
        
        StringBuilder sb = new StringBuilder();
        if(start)
            sb.append(linePoints.get(0).assymptoteString());
        else
            sb.append(linePoints.get(0).c2Assymptote());
        for(int q = 1; q < linePoints.size() - 1; q++)
            sb.append(linePoints.get(q).assymptoteString());
        if(end)
            sb.append(linePoints.get(linePoints.size()-1).assymptoteString());
        else
            sb.append(linePoints.get(linePoints.size()-1).c1Assymptote());
        
        return sb.toString();
    }
    public String tag(){
        return tag.positionAssymptote();
    }
    public String startPoint(){
        return linePoints.get(0).positionAssymptote();
    }
    public String endPoint(){
        if(linePoints.isEmpty()) return "";
        return linePoints.get(linePoints.size()-1).positionAssymptote();
    }

    public List<? extends TopoPoint> getLinePoints() {
        return linePoints;
    }
    
    public Point getStartPointImg(){
        if(linePoints.isEmpty()) return null;
        return linePoints.get(0).getLookupPosition();
    }
    public Point getEndPointImg(){
        if(linePoints.isEmpty()) return null;
        return linePoints.get(linePoints.size() - 1).getLookupPosition();
    }
    
    private class FindPoint implements BiConsumer<Integer, Point2D.Double> {
        Point2D.Double ret = null;
        Double distance = Double.MAX_VALUE;
        @Override
        public void accept(Integer t, Point2D.Double u){
            double dd = u.distanceSq(tag.getPosition());
            if(dd < distance){
                distance = dd;
                ret = u;
            }
        }

        public Point2D.Double getRet(){
            return ret;
        }
    }
    public Point2D.Double getNearestPointToTag(){
        FindPoint fp = new FindPoint();
        for(LineSegment ls : lineSegments)
            ls.forPoint(fp);
        
        return fp.getRet();
    }

    public void setTag(Point2D.Double tag){
        this.tag = new LinePoint(tag, null, null);
    }
    public Point2D.Double getTag(){
        if(isEmpty())
            return null;
        
        return tag.getPosition();
    }
    public Point getTagImg(){
        if(tag == null)
            return getStartPointImg();
        
        return tag.getLookupPosition();
    }
    
    public void updateLookupTable(){
        if(parent == null) return;
        
        lookupTableX = new int[lineSegments.size() * SECTION_SIZE];
        lookupTableY = new int[lineSegments.size() * SECTION_SIZE];
        
        for(LinePoint lp : linePoints)
            lp.updateLookup();
        if(!lineSegments.isEmpty()){
            int q = 0;  
            for(; q < lineSegments.size(); q++) 
                lineSegments.get(q).updateLookupTable(q * SECTION_SIZE);
            toLookup(lineSegments.get(q - 1).getPoint2().position, lookupTableX, lookupTableY, lookupTableX.length - 1);
        }
        if(tag != null)
            tag.updateLookup();
    }
      
    public void draw(Graphics g) {
        draw(g, colour);
    }
    public void draw(Graphics2D g2d){
        if(parent == null) return;
        
        g2d.drawPolyline(lookupTableX, lookupTableY, lookupTableX.length);
        
        if(tag != null){
            g2d.drawOval(tag.getLookupPosition().x - OVAL_SIZE, tag.getLookupPosition().y - OVAL_SIZE, OVAL_SIZE * 2, OVAL_SIZE * 2);
            int currPoint = 0, distance = Integer.MAX_VALUE;
            for(int q = 0; q < lookupTableX.length; q++){
                int dX = lookupTableX[q] - tag.getLookupPosition().x, dY = lookupTableY[q] - tag.getLookupPosition().y, dd = dX*dX + dY*dY;
                if(dd < distance){
                    distance = dd;
                    currPoint = q;
                }
            }
            g2d.drawLine(tag.getLookupPosition().x, tag.getLookupPosition().y, lookupTableX[currPoint], lookupTableY[currPoint]);
        }
        
        if(editable){
            for(LinePoint lp : linePoints)
                lp.draw(g2d);
            if(editLine.lineSegment != null)
                editLine.lineSegment.drawMouse(g2d);
            if(tag != null && tag.equals(editLine.getDragLP()))
                g2d.fillOval(tag.getLookupPosition().x - OVAL_SIZE, tag.getLookupPosition().y - OVAL_SIZE, OVAL_SIZE * 2, OVAL_SIZE * 2);
        }
        
        createLine.draw(g2d);
        
    }
    public void draw(Graphics g, Color colour) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke s = g2.getStroke();
        g2.setStroke(new BasicStroke(2));
        Color c = g2.getColor();
        g2.setColor(colour);
            
        draw(g2);
        
        g2.setStroke(s);
        g2.setColor(c);
    }

    public void setParent(Component parent, Point imgSize, Point offset, double scale) {
        this.parent = parent;
        this.offset.setLocation(offset);
        imageDimensions.setLocation(imgSize);
        this.scale = scale;
        setEditable(isEditable());
        updateLookupTable();
    }
    public void setScale(double scale){
        this.scale = scale;
        updateLookupTable();
    }
    public void setOffset(Point offset){
        this.offset.setLocation(offset);
        updateLookupTable();
    }
    private void drawLine(Graphics2D g, Point p1, Point p2){
        if(p1 == null || p2 == null) return;
        g.drawLine(p1.x, p1.y, p2.x, p2.y);
    }
    private void drawOval(Graphics2D g, Point p, boolean fill) {
        if(p == null) return;
        if(fill) g.fillOval(p.x - OVAL_SIZE_2, p.y - OVAL_SIZE_2, OVAL_SIZE, OVAL_SIZE);
        else g.drawOval(p.x - OVAL_SIZE_2, p.y - OVAL_SIZE_2, OVAL_SIZE, OVAL_SIZE);
    }
    private void drawOval(Graphics2D g, int x[], int y[], int pos, boolean fill) {
        if(fill) g.fillOval(x[pos] - OVAL_SIZE_2, y[pos] - OVAL_SIZE_2, OVAL_SIZE, OVAL_SIZE);
        else g.drawOval(x[pos] - OVAL_SIZE_2, y[pos] - OVAL_SIZE_2, OVAL_SIZE, OVAL_SIZE);
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        if(parent == null) return;
        
        // Minimise leakage
        removeMouseAdaptor(editLine);
        removeMouseAdaptor(createLine);
        
        if(editable)
            if(lineSegments.isEmpty()){
                removeMouseAdaptor(editLine);
                addMouseAdaptor(createLine);
            }else{
                removeMouseAdaptor(createLine);
                addMouseAdaptor(editLine);
            }
    }
    public boolean isEditable() {
        return editable;
    }

    public void setColour(Color colour) {
        this.colour = colour;
    }
    public Color getColour() {
        return colour;
    }
    
    public boolean isEmpty(){
        return linePoints.isEmpty();
    }
    public boolean hasImage(){
        return getTopoImage() != null && !getTopoImage().trim().isEmpty();
    }
    
    public void setTopoImage(String topoImage){
        this.topoImage.setValue(topoImage);
    }
    public String getTopoImage() {
        return topoImage.getValue();
    }

    public void parseXml(String xml) {
        linePoints.clear();
        lineSegments.clear();
        try {
            LinePoint last = null;
            for (String s : xml.split("and")) {
                LinePoint p = new LinePoint(s);
                linePoints.add(p);
                if (last != null) {
                    lineSegments.add(new LineSegment(last, p));
                }
                last = p;
            }
        } catch (Exception e) {
            System.out.println(e);
            linePoints.clear();
            lineSegments.clear();
        }
    }
    
    @Override
    protected void elementRoot(String uri, String localName, String qName, Attributes attributes) {
        topoImage.setValue(attributes.getValue("image"));
        parseXml(initalString = lastString = attributes.getValue("line"));
        uuid = attributes.getValue("uuid");
        
        String tagS = attributes.getValue("tag");
        if(tagS != null){
            String ss[] = tagS.split(",");
            tag = new LinePoint(new Point2D.Double(Double.parseDouble(ss[0]), Double.parseDouble(ss[1])), null, null);
        }else if(!isEmpty())
            tag = new LinePoint((Point2D.Double) linePoints.get(0).getPosition().clone(), null, null);
    }
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "image", "image", null, topoImage.getValue());
        ai.addAttribute(namespaceUri(), "line", "line", null, assymptoteString());
        ai.addAttribute(namespaceUri(), "uuid", "uuid", null, getUuid());
        if(tag != null)
            ai.addAttribute(namespaceUri(), "tag", "tag", null, String.valueOf(tag.getPosition().x) + "," + String.valueOf(tag.getPosition().y));
        return ai;
    }
    
    @Override
    public void setInitial(){
        initalString = assymptoteString();
        initialTag = tag==null?null:tag.getPosition();
    }
    @Override
    public void setLast(){
        lastString = assymptoteString();
        lastTag = tag==null?null:tag.getPosition();
    }
    @Override
    public void revertLast(){
        if(lastString == null) return;
        parseXml(lastString);
        updateLookupTable();
        tag = lastTag==null?null:new LinePoint(lastTag, null, null);
    }
    
    @Override
    public boolean hasChangedFromInitial(){    
        return !assymptoteString().equals(initalString) || !(tag==null?initialTag==null:tag.getPosition().equals(initialTag));
    }
    @Override
    public boolean hasChangedFromLast(){
        return !assymptoteString().equals(lastString) || !(tag==null?lastTag==null:tag.getPosition().equals(lastTag));
    }
    
    @Override
    public String getUuid() {
        if(uuid == null)
            uuid = UUID.randomUUID().toString();
        return uuid;
    }
}
