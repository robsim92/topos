/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class DiffCrag extends Diff{
    private final List<DiffArea> diffAreas = new ArrayList<>();
    private final List<DiffTextArea> diffTextAreas = new ArrayList<>();

    public DiffCrag(Crag c1, Crag c2) {
        super("Crag");
        
        getParameters().add(new DiffParameter<>(this, "Name", c1.name, c2.getName()));
        getParameters().add(new DiffParameter<>(this, "Topo Authors", c1.topoAuthors, c2.getTopoAuthors()));
        getParameters().add(new DiffParameter<>(this, "License", c1.license, c2.getLicense()));

        List<DiffListItem<TextArea>> textAreas = DiffListItem.listDiff(DiffTextArea.CREATOR, this, c1.getTextAreas(), c2.getTextAreas());
        getChildern().addAll(textAreas);
        for(DiffListItem<TextArea> dli : textAreas)
            diffTextAreas.add((DiffTextArea) dli);
        
        List<DiffListItem<Area>> areas = DiffListItem.listDiff(DiffArea.CREATOR, this, c1.getAreas(), c2.getAreas());
        getChildern().addAll(areas);
        for(DiffListItem<Area> a : areas)
            diffAreas.add((DiffArea) a);
    }

    public List<DiffArea> getDiffAreas(){
        return diffAreas;
    }
    public List<DiffTextArea> getDiffTextAreas(){
        return diffTextAreas;
    }
    
    @Override
    protected void acceptImpl() {
        acceptChildern();
        acceptParameters();
    }
}
