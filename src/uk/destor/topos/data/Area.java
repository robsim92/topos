/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.parameters.Changable;
import uk.destor.topos.data.parameters.Parameter;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class Area extends XmlParser implements Serializable, Changable, Unique{
    final Parameter<String> name = new Parameter<>(""), description = new Parameter<>("");
    private final List<TopoImage> topoImages = new ArrayList<>();
    private final List<TopoEntry> topoEntries = new ArrayList<>();
    private final List<String> topoImagesIndex = new ArrayList<>();
    private String uuid = null;
    private int topoHash;
        
    public Area() {
        super("Area");
        topoHash = topoEntries.hashCode();
    }

    @Override
    protected void elementRoot(String uri, String localName, String qName, Attributes attributes) {
        name.setValue(attributes.getValue("name"));
        uuid = attributes.getValue("uuid");
    }
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "name", "name", null, name.getValue());
        ai.addAttribute(namespaceUri(), "uuid", "uuid", null, getUuid());
        return ai;
    }

    @Override
    protected XmlParser startXmlElement(String uri, String localName, String qName, Attributes attributes) {
        if("Route".equals(qName)) return addToCollection(topoEntries, new TopoEntry());
        return null;
    }

    @Override
    protected void endXmlElement(String uri, String localName, String qName) {
        if("Description".equals(qName))
            description.setValue(builder.toString().trim());
        if("Area".equals(qName)){
            for (TopoEntry te : topoEntries) 
                for (TopoLine tl : te.getTopoLines().values())
                    if(tl != null){
                        String i = tl.getTopoImage();
                        if(i != null && !i.trim().isEmpty()){
                            if(!topoImagesIndex.contains(i)){
                                topoImagesIndex.add(i);
                                topoImages.add(new TopoImage(i));
                            }
                            topoImages.get(topoImagesIndex.lastIndexOf(i)).getTopoEntrys().add(te);
                        }
                    }
        }
    }
    @Override
    protected void outputChildren(ContentHandler contentHandler) throws SAXException {
        createXmlElements(contentHandler, topoEntries);
        if(description != null)
            createStringElement(contentHandler, "Description", description.getValue());
    }

    public String getName() {
        //Need to trim this to prevent problems on M$ systems due to whitespace at the end/start of the filenames
        return name.getValue().trim();
    }

    public void setName(String name) {
        this.name.setValue(name);
    }

    public String getDescription() {
        return description.getValue() == null ? "" : description.getValue();
    }

    public void setDescription(String description) {
        this.description.setValue(description);
    }

    public List<TopoImage> getTopoImages() {
        return topoImages;
    }

    public List<TopoEntry> getTopoEntries() {
        return topoEntries;
    }

    @Override
    public String getUuid() {
        if(uuid == null)
            uuid = UUID.randomUUID().toString();
        return uuid;
    }
    
    public void remove(TopoEntry te) {
        topoEntries.remove(te);
        for (TopoLine tl : te.getTopoLines().values()) 
            topoImages.get(topoImagesIndex.lastIndexOf(tl.getTopoImage())).getTopoEntrys().remove(te);
    }

    @Override
    public String toString() {
        return name.getValue();
    }

    public void updateTopoImagesIndex(){
        topoImagesIndex.clear();
        for(TopoImage ti : topoImages)
            topoImagesIndex.add(ti.getImageName());
    }
    public boolean hasTopoImage(String key){
        return topoImagesIndex.contains(key);
    }
    public TopoImage getTopoImage(String key) {
        if(!hasTopoImage(key)) return null;
        return topoImages.get(topoImagesIndex.lastIndexOf(key));
    }

    @Override
    public void setInitial() {
        topoHash = topoEntries.hashCode();
        name.setInitial();
        description.setInitial();
        for (TopoEntry te : topoEntries)
            te.setInitial();
    }

    @Override
    public boolean hasChangedFromInitial() {
        if(name.hasChangedFromInitial() || description.hasChangedFromInitial() || topoHash != topoEntries.hashCode())
            return true;
        
        for (TopoEntry te : topoEntries)
            if(te.hasChangedFromInitial())
                return true;
        
        return false;
    }

    @Override
    public void setLast() {
        name.setLast();
        description.setLast();
        for (TopoEntry te : topoEntries)
            te.setLast();
    }

    @Override
    public boolean hasChangedFromLast() {
        if(name.hasChangedFromLast() || description.hasChangedFromLast())
            return true;
        
        for (TopoEntry te : topoEntries)
            if(te.hasChangedFromLast())
                return true;
        
        return false;
    }

    @Override
    public void revertLast() {
        name.revertLast();
        description.revertLast();
        for (TopoEntry te : topoEntries)
            te.revertLast();
    }
    public void revertMeta(){
        name.revertLast();
        description.revertLast();
    }
}
