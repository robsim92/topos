/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data;

import java.awt.Image;
import java.util.UUID;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.parameters.Changable;
import uk.destor.topos.data.parameters.Parameter;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TextArea extends XmlParser implements Changable, Unique{
    final Parameter<String> name = new Parameter<>(""), text = new Parameter<>(""), imageName = new Parameter<>(""), caption = new Parameter<>("");
    final Parameter<Boolean> showTitle = new Parameter<>(false);
    private Image image;
    private String uuid = null;

    public TextArea() {
        super("Text");
    }
    
    @Override
    protected void elementRoot(String uri, String localName, String qName, Attributes attributes) {
        setName(attributes.getValue("name"));
        setImageName(attributes.getValue("image"));
        setCaption(attributes.getValue("caption"));
        String st = attributes.getValue("showTitle");
        showTitle(st == null || Boolean.parseBoolean(st)); // null check for backwards compatability
        uuid = attributes.getValue("uuid");
    }
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "name", "name", "CDATA", getName());
        ai.addAttribute(namespaceUri(), "image", "image", "CDATA", getImageName());
        ai.addAttribute(namespaceUri(), "caption", "caption", "CDATA", getCaption());
        ai.addAttribute(namespaceUri(), "showTitle", "showTitle", "CDATA", String.valueOf(showTitle()));
        ai.addAttribute(namespaceUri(), "uuid", "uuid", "CDATA", getUuid());
        return ai;
    }

    @Override
    protected void endXmlElement(String uri, String localName, String qName) {
        setText(builder.toString());
    }
    @Override
    protected void outputChildren(ContentHandler contentHandler) throws SAXException {
        contentHandler.characters(getText().toCharArray(), 0, getText().length());
    }

    public String getName() {
        //Need to trim this to prevent problems on M$ systems due to whitespace at the end/start of the filenames
        return name.getValue().trim();
    }
    public void setName(String name) {
        this.name.setValue(name);
    }
    public void showTitle(boolean show){
        showTitle.setValue(show);
    }
    public boolean showTitle(){
        return showTitle.getValue() == null ? true : showTitle.getValue();
    }
    public String getText() {
        return text.getValue() == null ? "" : text.getValue();
    }
    public void setText(String text) {
        this.text.setValue(text);
    }
    public String getImageName() {
        //Need to trim this to prevent problems on M$ systems due to whitespace at the end/start of the filenames
        return imageName.getValue() == null ? "" : imageName.getValue().trim();
    }
    public void setImageName(String image) {
        this.imageName.setValue(image);
    }
    public Image getImage() {
        return image;
    }
    public void setImage(Image image) {
        this.image = image;
    }
    public String getCaption() {
        return caption.getValue() == null ? "" : caption.getValue();
    }
    public void setCaption(String caption) {
        this.caption.setValue(caption);
    }
    public boolean hasImage(){
        return !getImageName().isEmpty();
    }

    @Override
    public String getUuid() {
        if(uuid == null)
            uuid = UUID.randomUUID().toString();
        return uuid;
    }
    
    @Override
    public String toString() {
        return getName();
    }

    @Override
    public void setInitial() {
        name.setInitial();
        text.setInitial();
        imageName.setInitial();
        caption.setInitial();
        showTitle.setInitial();
    }

    @Override
    public boolean hasChangedFromInitial() {
        return name.hasChangedFromInitial() || text.hasChangedFromInitial() || imageName.hasChangedFromInitial() || caption.hasChangedFromInitial() || showTitle.hasChangedFromInitial();
    }

    @Override
    public void setLast() {
        name.setLast();
        text.setLast();
        imageName.setLast();
        caption.setLast();
        showTitle.setLast();
    }

    @Override
    public boolean hasChangedFromLast() {
        return name.hasChangedFromLast() || text.hasChangedFromLast() || imageName.hasChangedFromLast() || caption.hasChangedFromLast() || showTitle.hasChangedFromLast();
    }

    @Override
    public void revertLast() {
        name.revertLast();
        text.revertLast();
        imageName.revertLast();
        caption.revertLast();
        showTitle.revertLast();
    }

}
