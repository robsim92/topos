/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.data.parameters;

import java.io.Serializable;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class Parameter<E> implements Serializable, Changable {
    
    private boolean initialSet = false, lastSet = false;
    private E value, initialValue, lastValue;
    private final E nullValue;

    public Parameter(E nullValue) {
        this.nullValue = nullValue;
        this.value = this.initialValue = this.lastValue = nullValue;
    }
    public Parameter(E nullValue, E value) {
        this.nullValue = nullValue;
        this.value = this.initialValue = this.lastValue = value;
        initialSet = lastSet = true;
    }
    
    @Override
    public boolean hasChangedFromLast(){
        return value != lastValue;
    }
    @Override
    public boolean hasChangedFromInitial(){
        return value != initialValue;
    }
    
    @Override
    public void setInitial(){
        initialValue = value;
        initialSet = true;
    }
    @Override
    public void setLast(){
        lastValue = value;
        lastSet = true;
    }
    @Override
    public void revertLast() {
        value = lastValue;
    }
    
    public void setValue(E value) {
        this.value = value==null ? nullValue : value;
        if(!initialSet)
            setInitial();
        if(!lastSet)
            setLast();
    }
    public E getValue() {
        return value;
    }
    public E getInitialValue() {
        return initialValue;
    }
    public E getLastValue() {
        return lastValue;
    }
    
}
