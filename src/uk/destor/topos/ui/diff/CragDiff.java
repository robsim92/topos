/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui.diff;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import uk.destor.topos.data.DiffArea;
import uk.destor.topos.data.DiffCrag;
import uk.destor.topos.data.DiffTextArea;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class CragDiff extends javax.swing.JDialog{    
    private class AreaListListener extends MouseAdapter{
        private final DiffListItemSelection<DiffArea> tes;

        public AreaListListener(DiffListItemSelection<DiffArea> tes){
            this.tes = tes;
        }
        
        @Override
        public void mouseClicked(MouseEvent e){
            for(DiffListItemSelection t : areas)
                t.setSelected(false);
            tes.setSelected(true);
            
            if(e.getClickCount() == 2){
                AreaDiff ad = new AreaDiff();
                ad.setDiffArea(tes.getE(), tes.getTitle());
                ad.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
                ad.setModalityType(ModalityType.APPLICATION_MODAL);
                ad.setVisible(true);
            }
            
            selectedArea = tes;
            switch(tes.getE().getStatus()){
                case EXISTING:
                    areaDiffPanel1.setArea(tes.getE(), true);
                    areaDiffPanel2.setArea(tes.getE(), false);
                    break;
                case NEW:
                    areaDiffPanel2.setArea(tes.getE().getItem(), true);
                    break;
                case DELETED:
                    areaDiffPanel1.setArea(tes.getE().getItem(), false);
                    break;
                case MOVED:
                    areaDiffPanel1.setArea(tes.getE().getItem(), false);
                    areaDiffPanel2.setArea(tes.getE().getItem(), false);
                    break;
                default:
                    throw new AssertionError();
            }
            
            repaint();
        }
    }    
    private class TextAreaListListener extends MouseAdapter{
        private final DiffListItemSelection<DiffTextArea> tes;

        public TextAreaListListener(DiffListItemSelection<DiffTextArea> tes){
            this.tes = tes;
        }
        
        @Override
        public void mouseClicked(MouseEvent e){
            for(DiffListItemSelection t : textAreas)
                t.setSelected(false);
            tes.setSelected(true);
            switch(tes.getE().getStatus()){
                case EXISTING:
                    textAreaDiff1.setTextArea(tes.getE(), true);
                    textAreaDiff2.setTextArea(tes.getE(), false);
                    break;
                case NEW:
                    textAreaDiff2.setTextArea(tes.getE().getItem(), true);
                    break;
                case DELETED:
                    textAreaDiff1.setTextArea(tes.getE().getItem(), false);
                    break;
                case MOVED:
                    textAreaDiff1.setTextArea(tes.getE().getItem(), false);
                    textAreaDiff2.setTextArea(tes.getE().getItem(), false);
                    break;
                default:
                    throw new AssertionError();
            }
            
            repaint();
        }
    }    
    private DiffCrag diffCrag;
    private DiffListItemSelection<DiffArea> selectedArea = null;
    
    private final List<DiffListItemSelection<DiffTextArea>> textAreas = new ArrayList<>();
    private final List<DiffListItemSelection<DiffArea>> areas = new ArrayList<>();
    
    /**
     * Creates new form DiffTopoImage
     */
    public CragDiff(){
        initComponents();
        textList.setLayout(new BoxLayout(textList, BoxLayout.Y_AXIS));
        areaList.setLayout(new BoxLayout(areaList, BoxLayout.Y_AXIS));
    }
    public void setDiffCrag(DiffCrag diffCrag){
        this.diffCrag = diffCrag;
        
        setTitle(diffCrag.getName());
        
        textList.removeAll();
        textAreas.clear();
        for(DiffTextArea dta : diffCrag.getDiffTextAreas()){
            DiffListItemSelection<DiffTextArea> tes = null;
            switch(dta.getStatus()){
                case EXISTING:
                    tes = new DiffListItemSelection<>(dta, dta.getName(false), dta.getName(true));
                    break;
                case NEW:
                    tes = new DiffListItemSelection<>(dta, null, dta.getItem().getName());
                    break;
                case DELETED:
                    tes = new DiffListItemSelection<>(dta, dta.getItem().getName(), null);
                    break;
                case MOVED:
                    tes = new DiffListItemSelection<>(dta, dta.getItem().getName(), dta.getItem().getName());
                    break;
                default:
                    throw new AssertionError();
            }
                
            tes.addMouseListener(new TextAreaListListener(tes));
            textList.add(tes);
            textAreas.add(tes);
        }
        
        areaList.removeAll();
        areas.clear();
        for(DiffArea da : diffCrag.getDiffAreas()){
            DiffListItemSelection<DiffArea> tes = null;
            switch(da.getStatus()){
                case EXISTING:
                    tes = new DiffListItemSelection<>(da, da.getName(false), da.getName(true));
                    break;
                case NEW:
                    tes = new DiffListItemSelection<>(da, null, da.getItem().getName());
                    break;
                case DELETED:
                    tes = new DiffListItemSelection<>(da, da.getItem().getName(), null);
                    break;
                case MOVED:
                    tes = new DiffListItemSelection<>(da, da.getItem().getName(), da.getItem().getName());
                    break;
                default:
                    throw new AssertionError();
            }
            tes.addMouseListener(new AreaListListener(tes));
            areaList.add(tes);
            areas.add(tes);
        }
        
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textList = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        textAreaDiff1 = new uk.destor.topos.ui.diff.TextAreaDiff();
        textAreaDiff2 = new uk.destor.topos.ui.diff.TextAreaDiff();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel4 = new javax.swing.JPanel();
        areaDiffPanel1 = new uk.destor.topos.ui.diff.AreaDiffPanel();
        areaDiffPanel2 = new uk.destor.topos.ui.diff.AreaDiffPanel();
        jButton3 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        areaList = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jButton1.setText("Close");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jSplitPane1.setDividerLocation(300);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Text Areas");

        jLabel2.setForeground(java.awt.Color.black);
        jLabel2.setText("Original");

        jLabel3.setForeground(java.awt.Color.black);
        jLabel3.setText("New");

        javax.swing.GroupLayout textListLayout = new javax.swing.GroupLayout(textList);
        textList.setLayout(textListLayout);
        textListLayout.setHorizontalGroup(
            textListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        textListLayout.setVerticalGroup(
            textListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 485, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 203, Short.MAX_VALUE)
                        .addComponent(jLabel3)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jPanel1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel3);

        textAreaDiff1.setName("Original"); // NOI18N

        textAreaDiff2.setName("New"); // NOI18N

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(textAreaDiff1, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textAreaDiff2, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(textAreaDiff1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(textAreaDiff2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jSplitPane1.setRightComponent(jPanel8);

        jTabbedPane1.addTab("Text Areas", jSplitPane1);

        jSplitPane2.setDividerLocation(300);

        areaDiffPanel1.setName("Original"); // NOI18N

        areaDiffPanel2.setName("New"); // NOI18N

        jButton3.setText("Routes");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(areaDiffPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(areaDiffPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(areaDiffPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
                    .addComponent(areaDiffPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addContainerGap())
        );

        jSplitPane2.setRightComponent(jPanel4);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Areas");

        jLabel4.setForeground(java.awt.Color.black);
        jLabel4.setText("Original");

        jLabel5.setForeground(java.awt.Color.black);
        jLabel5.setText("New");

        javax.swing.GroupLayout areaListLayout = new javax.swing.GroupLayout(areaList);
        areaList.setLayout(areaListLayout);
        areaListLayout.setHorizontalGroup(
            areaListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        areaListLayout.setVerticalGroup(
            areaListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 485, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(areaList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 203, Short.MAX_VALUE)
                        .addComponent(jLabel5)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(areaList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jScrollPane3.setViewportView(jPanel5);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane3)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE))
        );

        jSplitPane2.setLeftComponent(jPanel6);

        jTabbedPane1.addTab("Areas", jSplitPane2);

        jButton2.setText("Accept");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTabbedPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)))
                .addGap(0, 0, 0))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton1)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        for(DiffListItemSelection tes : textAreas)
            if(tes.isAccepted())
               tes.getE().accept();

        for(DiffListItemSelection tes : areas)
            if(tes.isAccepted())
                tes.getE().accept();
        
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if(selectedArea != null){
            AreaDiff ad = new AreaDiff();
            ad.setDiffArea(selectedArea.getE(), selectedArea.getTitle());
            ad.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
            ad.setModalityType(ModalityType.APPLICATION_MODAL);
            ad.setVisible(true);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]){
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try{
            for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
                if("Nimbus".equals(info.getName())){
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
        } catch(ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex){
            java.util.logging.Logger.getLogger(CragDiff.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable(){
            public void run(){
                new CragDiff().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private uk.destor.topos.ui.diff.AreaDiffPanel areaDiffPanel1;
    private uk.destor.topos.ui.diff.AreaDiffPanel areaDiffPanel2;
    private javax.swing.JPanel areaList;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private uk.destor.topos.ui.diff.TextAreaDiff textAreaDiff1;
    private uk.destor.topos.ui.diff.TextAreaDiff textAreaDiff2;
    private javax.swing.JPanel textList;
    // End of variables declaration//GEN-END:variables
}
