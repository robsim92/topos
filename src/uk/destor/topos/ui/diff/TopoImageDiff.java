/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui.diff;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import uk.destor.topos.data.Crag;
import uk.destor.topos.data.DiffArea;
import uk.destor.topos.data.DiffCrag;
import uk.destor.topos.data.DiffTopoLine;
import uk.destor.topos.data.TopoImage;
import uk.destor.topos.ui.TopoEditorBase;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoImageDiff extends javax.swing.JFrame{    
    private class ListListener extends MouseAdapter{
        private final TwinRadioBox tids;

        public ListListener(TwinRadioBox tids){
            this.tids = tids;
        }
        
        @Override
        public void mouseClicked(MouseEvent e){
            for(TwinRadioBox t : topoLineMap.values())
                t.setSelected(false);
            tids.setSelected(true);
            tids.getDiffTopoLine().getTopoLine1().setParent(topoEditor, topoEditor.getTopoImage().getImgSize(), topoEditor.getOffset(), topoEditor.getScale());
            tids.getDiffTopoLine().getTopoLine2().setParent(topoEditor, topoEditor.getTopoImage().getImgSize(), topoEditor.getOffset(), topoEditor.getScale());   
            tids.getDiffTopoLine().getTopoLine1().setEditable(false);
            tids.getDiffTopoLine().getTopoLine2().setEditable(true);
            selectedTopoLine = tids;
            repaint();
        }
    }    
    private TopoImage topoImage;
    private final List<DiffTopoLine> topoLines = new ArrayList<>();
    private final Map<DiffTopoLine, TwinRadioBox> topoLineMap = new HashMap<>();
    private final TopoEditorBase topoEditor = new TopoEditorBase(){
        @Override
        protected void paintLines(Graphics g){
            if(selectedTopoLine != null){
                selectedTopoLine.getDiffTopoLine().getTopoLine1().draw(g, Color.RED);
                selectedTopoLine.getDiffTopoLine().getTopoLine2().draw(g, Color.GREEN);
            }
        }

        @Override
        protected void resized(double scale, Point offset){
            if(selectedTopoLine != null){
                selectedTopoLine.getDiffTopoLine().getTopoLine1().setScale(scale);
                selectedTopoLine.getDiffTopoLine().getTopoLine2().setScale(scale);
                selectedTopoLine.getDiffTopoLine().getTopoLine1().setOffset(offset);
                selectedTopoLine.getDiffTopoLine().getTopoLine2().setOffset(offset);
            }
        }
    };   
    
    private TwinRadioBox selectedTopoLine = null;
    
    /**
     * Creates new form DiffTopoImage
     */
    public TopoImageDiff(){
        try{
            initComponents();
            jPanel4.setLayout(new BoxLayout(jPanel4, BoxLayout.Y_AXIS));
            jPanel2.setLayout(new BorderLayout());
            jPanel2.add(topoEditor);
            
            Crag crag1 = new Crag(new File("/home/rob/Starry_.tp"));
            Crag crag2 = new Crag(new File("/home/rob/Starry_1.tp"));
            
            DiffCrag diffCrag = new DiffCrag(crag1, crag2);
            DiffArea diffArea = diffCrag.getDiffAreas().get(1);
            String image = diffArea.getTopoLines().keySet().iterator().next();
            TopoImage ti = diffArea.getArea2().getTopoImage(image);
            setTopoImage(ti, diffArea.getTopoLines().get(image));
        } catch(IOException ex){
            Logger.getLogger(TopoImageDiff.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setTopoImage(TopoImage ti, List<DiffTopoLine> diffTopoLines){
        topoImage = ti;
        topoLines.clear();
        topoLines.addAll(diffTopoLines);
        
        topoEditor.setTopoImage(topoImage);
        
        jPanel4.removeAll();
        for(DiffTopoLine dtl : topoLines){
            TwinRadioBox tids = new TwinRadioBox(dtl);
            tids.addMouseListener(new ListListener(tids));
            topoLineMap.put(dtl, tids);
            jPanel4.add(tids);
        }
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jSplitPane1.setDividerLocation(250);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 712, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 749, Short.MAX_VALUE)
        );

        jSplitPane1.setRightComponent(jPanel2);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("jLabel1");

        jButton1.setText("Accept");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setForeground(java.awt.Color.red);
        jLabel2.setText("Original");

        jLabel3.setForeground(java.awt.Color.green);
        jLabel3.setText("New");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 663, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 153, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addContainerGap())
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton1))
            .addComponent(jScrollPane2)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1))
        );

        jSplitPane1.setLeftComponent(jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        for(DiffTopoLine dtl : topoLines)
            if(topoLineMap.get(dtl).isAccepted())
                dtl.accept();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]){
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try{
            for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
                if("Nimbus".equals(info.getName())){
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
        } catch(ClassNotFoundException ex){
            java.util.logging.Logger.getLogger(TopoImageDiff.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch(InstantiationException ex){
            java.util.logging.Logger.getLogger(TopoImageDiff.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch(IllegalAccessException ex){
            java.util.logging.Logger.getLogger(TopoImageDiff.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch(javax.swing.UnsupportedLookAndFeelException ex){
            java.util.logging.Logger.getLogger(TopoImageDiff.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable(){
            public void run(){
                new TopoImageDiff().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    // End of variables declaration//GEN-END:variables
}
