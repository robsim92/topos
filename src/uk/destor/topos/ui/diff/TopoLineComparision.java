/*
 * Copyright (C) 2020 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui.diff;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import uk.destor.topos.data.DiffTopoLine;
import uk.destor.topos.data.TopoImage;
import uk.destor.topos.data.TopoLine;
import uk.destor.topos.ui.TopoEditorBase;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoLineComparision extends TopoEditorBase{
    private DiffTopoLine diffTopoLine = null;

    public void setTopoLineDiff(DiffTopoLine diffTopoLine, TopoImage ti){
        this.diffTopoLine = diffTopoLine;
        setTopoImage(ti);
        if(diffTopoLine != null){
            if(diffTopoLine.getTopoLine1() != null)
                diffTopoLine.getTopoLine1().setParent(this, ti.getImgSize(), offset, scale);
            if(diffTopoLine.getTopoLine2() != null)
                diffTopoLine.getTopoLine2().setParent(this, ti.getImgSize(), offset, scale);
        }
    }

    @Override
    protected void resized(double scale, Point offset){
        if(diffTopoLine != null){
            if(diffTopoLine.getTopoLine1() != null){
                diffTopoLine.getTopoLine1().setScale(scale);
                diffTopoLine.getTopoLine1().setOffset(offset);   
            }
            if(diffTopoLine.getTopoLine2() != null){
                diffTopoLine.getTopoLine2().setScale(scale);
                diffTopoLine.getTopoLine2().setOffset(offset);
            }
        }
    }

    @Override
    protected void paintLines(Graphics g){
        if(diffTopoLine != null){
            Graphics2D g2d = (Graphics2D) g.create();
            if(diffTopoLine.getTopoLine2() == null){
                g2d.setStroke(new BasicStroke(2));
                drawLine(g2d, diffTopoLine.getTopoLine1(), "Deleted", Color.RED, true);
            }else{
                if(diffTopoLine.getTopoLine1() != null){
                    g2d.setStroke(new BasicStroke(2));
                    drawLine(g2d, diffTopoLine.getTopoLine1(), "Original", Color.MAGENTA, true);
                    g2d.setStroke(new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));
                    drawLine(g2d, diffTopoLine.getTopoLine2(), "New", Color.GREEN, false);
                }else{
                    g2d.setStroke(new BasicStroke(2));
                    drawLine(g2d, diffTopoLine.getTopoLine2(), "New", Color.GREEN, false);
                }
            }
            g2d.dispose();
        }
    }
    private void drawLine(Graphics2D g, TopoLine tl, String name, Color c, boolean textAtStart){
        Color c1 = g.getColor();
        g.setColor(c);
        tl.draw(g);
        Point p = textAtStart ? tl.getStartPointImg() : tl.getEndPointImg();
        g.drawChars(name.toCharArray(), 0, name.length(), p.x + 10, p.y + 10);
        g.setColor(c1);
    }
}
