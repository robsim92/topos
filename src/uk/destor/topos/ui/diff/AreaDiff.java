/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui.diff;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import uk.destor.topos.data.DiffArea;
import uk.destor.topos.data.DiffTopoEntry;
import uk.destor.topos.data.DiffTopoLine;
import uk.destor.topos.data.TopoImage;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class AreaDiff extends javax.swing.JDialog{    
    private class ListListener extends MouseAdapter{
        private final DiffListItemSelection<DiffTopoEntry> tes;

        public ListListener(DiffListItemSelection<DiffTopoEntry> tes){
            this.tes = tes;
        }
        
        @Override
        public void mouseClicked(MouseEvent e){
            for(DiffListItemSelection t : topoEntries)
                t.setSelected(false);
            tes.setSelected(true);
            
            listModel.clear();
            for(DiffTopoLine dtl : tes.getE().getTopoLines())
                if(!dtl.getStatus().equals(DiffTopoLine.Status.EXISTING_SAME))
                    listModel.addElement(dtl);
            
            if(listModel.isEmpty()){
                jList1.clearSelection();
                topoLineComparision1.setTopoLineDiff(null, null);
            } else
                jList1.setSelectedIndex(0);
            
            switch(tes.getE().getStatus()){
                case EXISTING:
                    topoDiff1.setDiffTopoEntry(tes.getE());
                    break;
                case NEW:
                    topoDiff1.setTopoEntry(tes.getE().getItem(), true);
                    break;
                case DELETED:
                    topoDiff1.setTopoEntry(tes.getE().getItem(), false);
                    break;
                case MOVED:
                    topoDiff1.setTopoEntry(tes.getE().getItem(), true);
                    break;
                default:
                    throw new AssertionError();
            }

            repaint();
        }
    }    
    private DiffArea diffArea;
    private final List<DiffListItemSelection> topoEntries = new ArrayList<>();
    private final DefaultListModel<DiffTopoLine> listModel = new DefaultListModel<>();
    
    /**
     * Creates new form DiffTopoImage
     */
    public AreaDiff(){
        initComponents();
        jPanel4.setLayout(new BoxLayout(jPanel4, BoxLayout.Y_AXIS));
        jList1.setCellRenderer(new ListCellRenderer<DiffTopoLine>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends DiffTopoLine> list, DiffTopoLine value, int index, boolean isSelected, boolean cellHasFocus){
                float width = jList1.getWidth();
                TopoImage ti = getTopoImage(value);
                if(ti.getAspectRatio() != 0)
                    ti.setPreferredSize(new Dimension((int) width, (int) (width/ti.getAspectRatio())));
                return ti;
            }
        });
        jList1.setModel(listModel);
    }
    
    public void setDiffArea(DiffArea diffArea, String title){
        this.diffArea = diffArea;
        setTitle(title);
        
        jPanel4.removeAll();
        for(DiffTopoEntry dte : diffArea.getDiffTopoEntrys()){
            DiffListItemSelection<DiffTopoEntry> tes = null;
            switch(dte.getStatus()){
                case EXISTING:
                    tes = new DiffListItemSelection<>(dte, dte.getName(false), dte.getName(true));
                    break;
                case NEW:
                    tes = new DiffListItemSelection<>(dte, null, dte.getItem().getName());
                    break;
                case DELETED:
                    tes = new DiffListItemSelection<>(dte, dte.getItem().getName(), null);
                    break;
                case MOVED:
                    tes = new DiffListItemSelection<>(dte, dte.getItem().getName(), dte.getItem().getName());
                    break;
                default:
                    throw new AssertionError();
            }
            tes.addMouseListener(new ListListener(tes));
            topoEntries.add(tes);
            jPanel4.add(tes);
        }
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        topoDiff1 = new uk.destor.topos.ui.diff.TopoDiff();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        topoLineComparision1 = new uk.destor.topos.ui.diff.TopoLineComparision();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jSplitPane1.setDividerLocation(300);

        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(topoDiff1, javax.swing.GroupLayout.DEFAULT_SIZE, 822, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(topoLineComparision1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(topoDiff1, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(topoLineComparision1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        jSplitPane1.setRightComponent(jPanel2);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Routes/Problems");

        jButton1.setText("Accept");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setForeground(java.awt.Color.black);
        jLabel2.setText("Original");

        jLabel3.setForeground(java.awt.Color.black);
        jLabel3.setText("New");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 668, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 203, Short.MAX_VALUE)
                        .addComponent(jLabel3)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton1))
            .addComponent(jScrollPane2)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1))
        );

        jSplitPane1.setLeftComponent(jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(diffArea.getArea1() == null)
            diffArea.getArea2().getTopoEntries().clear();
        else
            diffArea.getArea1().getTopoEntries().clear();
        for(DiffListItemSelection tes : topoEntries){
            tes.getE().reset();
            if(tes.isAccepted())
                tes.getE().accept();
            else
                tes.getE().reject();
        }
            
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
        if(jList1.isSelectionEmpty())
            return;
        
        topoLineComparision1.setTopoLineDiff(jList1.getSelectedValue(), getTopoImage(jList1.getSelectedValue()));
    }//GEN-LAST:event_jList1ValueChanged

    private TopoImage getTopoImage(DiffTopoLine line){
        if(line.getStatus().equals(DiffTopoLine.Status.EXISTING_CHANGED) || line.getStatus().equals(DiffTopoLine.Status.NEW))
            return diffArea.getArea2().getTopoImage(line.getTopoLine2().getTopoImage());
        else
            return diffArea.getArea1().getTopoImage(line.getTopoLine1().getTopoImage());
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]){
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try{
            for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
                if("Nimbus".equals(info.getName())){
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
        } catch(ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex){
            java.util.logging.Logger.getLogger(AreaDiff.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable(){
            public void run(){
                new AreaDiff().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JList<DiffTopoLine> jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private uk.destor.topos.ui.diff.TopoDiff topoDiff1;
    private uk.destor.topos.ui.diff.TopoLineComparision topoLineComparision1;
    // End of variables declaration//GEN-END:variables
}
