/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import javax.swing.JComponent;
import uk.destor.topos.data.TopoImage;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public abstract class TopoEditorBase extends JComponent{
    protected TopoImage topoImage = null;
    protected double scale = 1;
    protected final Point offset = new Point(0, 0);
    protected final Point lastSize;
      
    private final MouseAdapter mouseAdapter = new MouseAdapter() {
        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            double oldScale = scale;
            if(e.getWheelRotation() > 0)
                scale /= 1.1;
            else
                scale *= 1.1;
            
            checkScale();
            offset.x = (int)(scale*((double)offset.x -  e.getX())/oldScale) + e.getX();
            offset.y = (int)(scale*((double)offset.y -  e.getY())/oldScale) + e.getY();
            checkOffset();
            
            if(topoImage != null)
                resized(scale, offset);
            
            repaint();
        }

        private final Point clickPosition = new Point();
        @Override
        public void mousePressed(MouseEvent e) {
            clickPosition.setLocation(e.getPoint());
        }
        @Override
        public void mouseDragged(MouseEvent e) {
            offset.x += e.getX() - clickPosition.x;
            offset.y += e.getY() - clickPosition.y;
            clickPosition.setLocation(e.getPoint());
            
            checkOffset();
            
            if(topoImage != null)
                topoImage.getTopoEntrys().forEach((te) -> {
                    te.getTopoLine(topoImage.getImageName()).setOffset(offset);
                });
            
            repaint();
        }
    };


    public TopoEditorBase(){
        addMouseWheelListener(mouseAdapter);
        addMouseListener(mouseAdapter);
        
        lastSize = new Point(getWidth(), getHeight());
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                if(topoImage != null){
                    checkScale();
                    offset.x += (getWidth() - lastSize.x)/2;
                    offset.y += (getHeight() - lastSize.y)/2;
                    checkOffset();
                    
                    lastSize.setLocation(getWidth(), getHeight());
                    
                    resized(scale, offset);
                }
                                
                repaint();
            } 
        });
        
    }
    protected abstract void resized(double scale, Point offset);
    
    public void setTopoImage(TopoImage topoImage){
        this.topoImage = topoImage;
        if(topoImage != null){
            scale = Math.min(getWidth() / (double) topoImage.getImgSize().x, getHeight() / (double) topoImage.getImgSize().y);
            offset.x = (int) ((getWidth() - scale * topoImage.getImgSize().x) / 2);
            offset.y = (int) ((getHeight() - scale * topoImage.getImgSize().y) / 2);
        }
        repaint();
    }

    public void clearImage(){
        topoImage = null;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        if(topoImage == null)
            return;
        
        if(topoImage.getImage() != null)
            g.drawImage(topoImage.getImage(), offset.x, offset.y, (int) (scale * topoImage.getImage().getWidth(this)), (int) (scale * topoImage.getImage().getHeight(this)), this);
        paintLines(g);
    }
    protected abstract void paintLines(Graphics g);

    public TopoImage getTopoImage(){
        return topoImage;
    }
    public Point getOffset(){
        return offset;
    }
    public double getScale(){
        return scale;
    }

    protected void checkScale(){
        if(topoImage != null && scale * topoImage.getImgSize().x < getWidth() && scale * topoImage.getImgSize().y < getHeight())
            scale = Math.min(getWidth() / (double) topoImage.getImgSize().x, getHeight() / (double) topoImage.getImgSize().y);
    }

    protected void checkOffset(){
        if(scale * topoImage.getImgSize().x < getWidth())
            offset.x = (int) ((getWidth() - scale * topoImage.getImgSize().x) / 2);
        else if(offset.x > 0)
            offset.x = 0;
        else if(offset.x < -scale * topoImage.getImgSize().x + getWidth())
            offset.x = (int) (-scale * topoImage.getImgSize().x + getWidth());
        if(scale * topoImage.getImgSize().y < getHeight())
            offset.y = (int) ((getHeight() - scale * topoImage.getImgSize().y) / 2);
        else if(offset.y > 0)
            offset.y = 0;
        else if(offset.y < -scale * topoImage.getImgSize().y + getHeight())
            offset.y = (int) (-scale * topoImage.getImgSize().y + getHeight());
    }

    @Override
    //Need to stop drag events overriding line edits.
    protected void processMouseMotionEvent(MouseEvent e) {
        super.processMouseMotionEvent(e);
        if(!e.isConsumed()){
            switch(e.getID()){
              case MouseEvent.MOUSE_MOVED:
                  mouseAdapter.mouseMoved(e);
                  break;
              case MouseEvent.MOUSE_DRAGGED:
                  mouseAdapter.mouseDragged(e);
                  break;
            }
        }
    }
       
}
