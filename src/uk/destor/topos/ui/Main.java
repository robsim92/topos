/*
 * Copyright (C) 2019 rob
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui;

import java.awt.Dialog;
import uk.destor.topos.ui.diff.CragDiff;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.transform.TransformerException;
import uk.destor.topos.data.Area;
import uk.destor.topos.data.Crag;
import uk.destor.topos.data.DiffCrag;
import uk.destor.topos.data.TextArea;
import uk.destor.topos.output.LatexOutput;
import uk.destor.topos.output.xslt.CragXslt;

//TODO: Ask to save new route when moving image
//TODO: Auto-saving
/**
 * @author rob
 */
public class Main extends javax.swing.JFrame {
    private Crag crag = new Crag();
    private File cragFile = null;
    
    private ListModelImpl<Area> areaListModel;
    private ListModelImpl<TextArea> textListModel;
            
    private Area openArea = null;
    private TextArea openText = null;
    
    /**
     * Creates new form Main
     */
    public Main() {
        try{
            Logger l = Logger.getLogger("uk.destor.topos");
            File jar = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            FileHandler fh = new FileHandler(new File(jar.getParentFile(), "Error.log").getAbsolutePath(), false);
            fh.setFormatter(new SimpleFormatter());
            l.addHandler(fh);
            fh.publish(new LogRecord(Level.INFO, "Welcome to the Gibson File. Please submit errors to: https://bitbucket.org/robsim92/topos"));        
        } catch(IOException | SecurityException | URISyntaxException ex){
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        initComponents();
        areaList.setModel(areaListModel = new ListModelImpl<>(crag.getAreas()));
        textList.setModel(textListModel = new ListModelImpl<>(crag.getTextAreas()));
        enableDnD();
    }

    private void openArea() {
        if(areaList.getSelectedValue() == null) return;
        areaNameText.setText(areaList.getSelectedValue().getName());
        areaDescriptionText.setText(areaList.getSelectedValue().getDescription());
        openArea = areaList.getSelectedValue();
    }
    private void enableAreaUI(boolean enable) {
        areaDescriptionText.setEnabled(enable);
        areaNameText.setEnabled(enable);
        editRoutesButton.setEnabled(enable);
        resetAreaButton.setEnabled(enable);
    }
    private void openText() {
        if(textList.getSelectedValue() == null) return;
        
        openText = textList.getSelectedValue();
        titleText.setText(openText.getName());
        sectionText.setText(openText.getText());
        removeTextImageButton.setEnabled(openText.hasImage());
        if (openText.hasImage()) {
            imageText.setText(openText.getImageName());
            captionText.setText(openText.getCaption());
        } else {
            imageText.setText("");
            captionText.setText("");
        }
        removeTextImageButton.setEnabled(openText.hasImage());
        captionText.setEditable(openText.hasImage());

        jCheckBox1.setSelected(openText.showTitle());
    }
    private void enableTextUI(boolean enable) {
        titleText.setEnabled(enable);
        sectionText.setEnabled(enable);
        resetTextButton.setEnabled(enable);
        browseButton.setEnabled(enable);
        if (!enable) {
            removeTextImageButton.setEnabled(enable);
            captionText.setEditable(enable);
        }
    }
    private void saveAs(File f) {
        if (f == null) {
            JFileChooser chooser = new JFileChooser();
            chooser.setDialogTitle("Select file to save.");
            chooser.setFileFilter(new FileNameExtensionFilter("Topo Files", "tp"));
            if (chooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) {
                return;
            }
            f = chooser.getSelectedFile();
            if(!f.getAbsolutePath().endsWith(".tp"))
                f = new File(f.getAbsolutePath() + ".tp");
            setTitle(crag.getName() + "(" + chooser.getSelectedFile().getAbsolutePath() + ")");
        }
        try{
            cragFile = f;
            crag.writeToZip(new FileOutputStream(cragFile));
            crag.setInitial();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        titleText = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        sectionText = new javax.swing.JTextArea();
        resetTextButton = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        imageText = new javax.swing.JTextField();
        browseButton = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        captionText = new javax.swing.JTextField();
        removeTextImageButton = new javax.swing.JButton();
        jCheckBox1 = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        deleteTextButton = new javax.swing.JButton();
        newTextButton = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        textList = new javax.swing.JList<>();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        areaNameText = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        areaDescriptionText = new javax.swing.JTextArea();
        editRoutesButton = new javax.swing.JButton();
        resetAreaButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        deleteAreaButton = new javax.swing.JButton();
        newAreaButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        areaList = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                Main.this.windowClosing(evt);
            }
        });

        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);

        jSplitPane2.setDividerLocation(200);

        jLabel4.setText("Section Title:");

        titleText.setEnabled(false);
        titleText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                titleTextKeyReleased(evt);
            }
        });

        jLabel5.setText("Section Text:");

        sectionText.setColumns(20);
        sectionText.setLineWrap(true);
        sectionText.setRows(5);
        sectionText.setWrapStyleWord(true);
        sectionText.setEnabled(false);
        sectionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                sectionTextKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(sectionText);

        resetTextButton.setText("Reset");
        resetTextButton.setEnabled(false);
        resetTextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetTextButtonActionPerformed(evt);
            }
        });

        jLabel6.setText("Image File:");

        imageText.setEditable(false);

        browseButton.setText("Browse");
        browseButton.setEnabled(false);
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseButtonActionPerformed(evt);
            }
        });

        jLabel7.setText("Caption:");

        captionText.setEditable(false);
        captionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                captionTextKeyReleased(evt);
            }
        });

        removeTextImageButton.setText("Remove");
        removeTextImageButton.setEnabled(false);
        removeTextImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeTextImageButtonActionPerformed(evt);
            }
        });

        jCheckBox1.setSelected(true);
        jCheckBox1.setText("Show Title");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(captionText)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(imageText, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(removeTextImageButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(browseButton))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(resetTextButton))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(titleText)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBox1)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(titleText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(imageText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browseButton)
                    .addComponent(removeTextImageButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(captionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resetTextButton)
                .addGap(6, 6, 6))
        );

        jSplitPane2.setRightComponent(jPanel4);

        deleteTextButton.setText("Delete");
        deleteTextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteTextButtonActionPerformed(evt);
            }
        });

        newTextButton.setText("New");
        newTextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newTextButtonActionPerformed(evt);
            }
        });

        textList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        textList.setDragEnabled(true);
        textList.setDropMode(javax.swing.DropMode.INSERT);
        textList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                textListValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(textList);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(deleteTextButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(newTextButton))
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteTextButton)
                    .addComponent(newTextButton)))
        );

        jSplitPane2.setLeftComponent(jPanel5);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 899, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(jSplitPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 899, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 561, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(jSplitPane2)
                    .addGap(0, 0, 0)))
        );

        jTabbedPane1.addTab("Crag Description", jPanel3);

        jSplitPane1.setDividerLocation(200);

        jLabel2.setText("Area Name:");

        areaNameText.setEnabled(false);
        areaNameText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                areaNameTextKeyReleased(evt);
            }
        });

        jLabel3.setText("Area Description:");

        areaDescriptionText.setColumns(20);
        areaDescriptionText.setLineWrap(true);
        areaDescriptionText.setRows(5);
        areaDescriptionText.setWrapStyleWord(true);
        areaDescriptionText.setEnabled(false);
        areaDescriptionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                areaDescriptionTextKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(areaDescriptionText);

        editRoutesButton.setText("Edit Routes/Problems");
        editRoutesButton.setEnabled(false);
        editRoutesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editRoutesButtonActionPerformed(evt);
            }
        });

        resetAreaButton.setText("Reset");
        resetAreaButton.setEnabled(false);
        resetAreaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetAreaButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(editRoutesButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(resetAreaButton))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 664, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(areaNameText)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(areaNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(resetAreaButton)
                    .addComponent(editRoutesButton)))
        );

        jSplitPane1.setRightComponent(jPanel1);

        deleteAreaButton.setText("Delete");
        deleteAreaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteAreaButtonActionPerformed(evt);
            }
        });

        newAreaButton.setText("New");
        newAreaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newAreaButtonActionPerformed(evt);
            }
        });

        areaList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        areaList.setDragEnabled(true);
        areaList.setDropMode(javax.swing.DropMode.INSERT);
        areaList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                areaListMouseClicked(evt);
            }
        });
        areaList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                areaListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(areaList);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(deleteAreaButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(newAreaButton))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteAreaButton)
                    .addComponent(newAreaButton)))
        );

        jSplitPane1.setLeftComponent(jPanel2);

        jTabbedPane1.addTab("Routes", jSplitPane1);

        jMenu1.setMnemonic('f');
        jMenu1.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setMnemonic('n');
        jMenuItem1.setText("New");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setMnemonic('o');
        jMenuItem2.setText("Open...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);
        jMenu1.add(jSeparator1);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setMnemonic('s');
        jMenuItem3.setText("Save");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setMnemonic('a');
        jMenuItem4.setText("Save As...");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);
        jMenu1.add(jSeparator2);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setMnemonic('c');
        jMenuItem5.setText("Close");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuBar1.add(jMenu1);

        jMenu3.setMnemonic('c');
        jMenu3.setText("Crag Info");

        jMenuItem7.setText("Crag Name");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem7);

        jMenuItem8.setText("Topo Authors");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenuItem10.setText("Topo License");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem10);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Compare");

        jMenuItem11.setText("Merge Topo");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem11);

        jMenuBar1.add(jMenu4);

        jMenu2.setText("Export");

        jMenuItem6.setText("LaTeX");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        jMenuItem12.setText("XML File");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem12);

        jMenuItem9.setText("PDF");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem9);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jTabbedPane1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void deleteAreaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteAreaButtonActionPerformed
        if(areaList.getSelectedValue() == null)
            return;

        areaListModel.remove(areaList.getSelectedValue());
        enableAreaUI(false);
    }//GEN-LAST:event_deleteAreaButtonActionPerformed

    private void newAreaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newAreaButtonActionPerformed
        String name = JOptionPane.showInputDialog("Enter the name of the area.");
        if (name != null && !name.trim().isEmpty()) {
            openArea = new Area();
            openArea.setName(name);
            areaNameText.setText(name);
            areaListModel.add(openArea);
            areaList.setSelectedValue(openArea, true);
            enableAreaUI(true);
        }
    }//GEN-LAST:event_newAreaButtonActionPerformed

    private void areaListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_areaListValueChanged
        openArea();
        enableAreaUI(true);
    }//GEN-LAST:event_areaListValueChanged

    private void resetAreaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetAreaButtonActionPerformed
        openArea.revertMeta();
        openArea();
    }//GEN-LAST:event_resetAreaButtonActionPerformed

    private void editRoutesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editRoutesButtonActionPerformed
        new AreaEditor(areaList.getSelectedValue()).setVisible(true);
    }//GEN-LAST:event_editRoutesButtonActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        if(crag.hasChangedFromInitial()){
            int i = JOptionPane.showConfirmDialog(this, "Save any changes to the crag?", "New Crag", JOptionPane.YES_NO_CANCEL_OPTION);
            if(i == JOptionPane.YES_OPTION)
                saveAs(cragFile);
            else if(i == JOptionPane.CANCEL_OPTION)
                return;
        }

        crag = new Crag();
        openArea = null;
        openText = null;
        areaList.setModel(areaListModel = new ListModelImpl<>(crag.getAreas()));
        textList.setModel(textListModel = new ListModelImpl<>(crag.getTextAreas()));
        
        crag.setName(JOptionPane.showInputDialog("Enter the name of crag."));
        setTitle(crag.getName() + " ()");
        
        enableDnD();
        
        enableAreaUI(false);
        enableTextUI(false);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        if(crag.hasChangedFromInitial()){
            int i = JOptionPane.showConfirmDialog(this, "Save any changes to the crag?", "Open Crag", JOptionPane.YES_NO_CANCEL_OPTION);
            if(i == JOptionPane.YES_OPTION)
                saveAs(cragFile);
            else if(i == JOptionPane.CANCEL_OPTION)
                return;
        }

        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select file to open.");
        chooser.setFileFilter(new FileNameExtensionFilter("Topo  Files", "tp"));
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {        
            openFile(chooser.getSelectedFile());
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    public void openFile(File file){
        try{
            crag = new Crag(file);
            openCrag(crag);
            setTitle(crag.getName() + " (" + file.getAbsolutePath() + ")");
            cragFile = file;
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void openCrag(Crag crag){
        openArea = null;
        openText = null;
        areaList.setModel(areaListModel = new ListModelImpl<>(crag.getAreas()));
        textList.setModel(textListModel = new ListModelImpl<>(crag.getTextAreas()));
        
        enableDnD();
        
        enableAreaUI(!areaListModel.isEmpty());
        enableTextUI(!textListModel.isEmpty());
        
        if (!areaListModel.isEmpty())
            areaList.setSelectedIndex(0);
        if (!textListModel.isEmpty())
            textList.setSelectedIndex(0);
    }

    public void enableDnD() {
        try {
            areaList.setTransferHandler(new Transfer<>(Area.class, areaList, areaListModel));
            textList.setTransferHandler(new Transfer<>(TextArea.class, textList, textListModel));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        saveAs(cragFile);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        saveAs(null);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        windowClosing(null);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        //TODO: Ask if they want to use the default template.
        
        JFileChooser inChooser = new JFileChooser(cragFile);
        inChooser.setDialogTitle("Select template file");
        inChooser.setFileFilter(new FileNameExtensionFilter("LaTeX Files", "tex"));
        if (inChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            JFileChooser outChooser = new JFileChooser();
            outChooser.setDialogTitle("Output file");
            outChooser.setFileFilter(new FileNameExtensionFilter("LaTeX Files", "tex"));
            outChooser.setCurrentDirectory(inChooser.getCurrentDirectory());
            if (outChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                if (inChooser.getSelectedFile() == outChooser.getSelectedFile()) {
                    JOptionPane.showMessageDialog(this, "The output file must not be the tempalte file.");
                    return;
                }
                try {
                    LatexOutput output = new LatexOutput(crag);
                    output.setTemplate(new FileInputStream(inChooser.getSelectedFile()));
                    
                    File f = outChooser.getSelectedFile();
                    if(!f.getAbsolutePath().endsWith(".tex"))
                        f = new File(f.getAbsolutePath() + ".tex");
                    crag.createOutputFolder(f.getParentFile(), output);
                    output.createOutput(new FileOutputStream(f));
                    JOptionPane.showMessageDialog(this, "The output was successful.");
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(this, "The output was failed due to " + ex.getLocalizedMessage() + ".");
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void resetTextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetTextButtonActionPerformed
        openText.revertLast();
        openText();
    }//GEN-LAST:event_resetTextButtonActionPerformed

    private void deleteTextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteTextButtonActionPerformed
        TextArea ta = textList.getSelectedValue();
        if (ta == null) return;
        
        textListModel.remove(ta);
        enableTextUI(false);        
    }//GEN-LAST:event_deleteTextButtonActionPerformed

    private void newTextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newTextButtonActionPerformed
        String name = JOptionPane.showInputDialog("Enter the title.");
        if (name != null && !name.trim().isEmpty()) {
            openText = new TextArea();
            openText.setName(name);
            textListModel.add(openText);
            textList.setSelectedValue(openText, true);
            enableTextUI(true);
            openText();
        }

    }//GEN-LAST:event_newTextButtonActionPerformed

    private void textListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_textListValueChanged
        openText();
        enableTextUI(true);
    }//GEN-LAST:event_textListValueChanged

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseButtonActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select Image to Add.");
        chooser.setFileFilter(new FileNameExtensionFilter("Image files", "jpeg", "jpg", "png", "bmp"));
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            imageText.setText(chooser.getSelectedFile().getAbsolutePath());
            removeTextImageButton.setEnabled(true);
            captionText.setEditable(true);
        }
    }//GEN-LAST:event_browseButtonActionPerformed

    private void removeTextImageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeTextImageButtonActionPerformed
        imageText.setText("");
        captionText.setText("");
        removeTextImageButton.setEnabled(false);
        captionText.setEditable(false);
    }//GEN-LAST:event_removeTextImageButtonActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        String name = JOptionPane.showInputDialog(this, "Enter the name of crag.", crag.getName());
        if(name != null){
            crag.setName(name);
            setTitle(crag.getName() + " (" + (cragFile == null ? "" : cragFile.getAbsolutePath()) + ")");
        }
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        String ta = JOptionPane.showInputDialog(this, "Enter the names of the authors.", crag.getTopoAuthors());
        if(ta != null)
            crag.setTopoAuthors(ta);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void windowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_windowClosing
        if(crag.hasChangedFromInitial()){
            int i = JOptionPane.showConfirmDialog(this, "Save any changes to the crag?", "Closing", JOptionPane.YES_NO_CANCEL_OPTION);
            if(i == JOptionPane.YES_OPTION)
                saveAs(cragFile);
            if(i != JOptionPane.CANCEL_OPTION)
                System.exit(0);
        }else
            System.exit(0);
    }//GEN-LAST:event_windowClosing

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        new CreatePdf(this, true, crag).setVisible(true);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        new LicenseDialog(this, crag).setVisible(true);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void areaListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_areaListMouseClicked
        if(evt.getClickCount() == 2 && areaList.getSelectedValue() != null)
            new AreaEditor(areaList.getSelectedValue()).setVisible(true);
    }//GEN-LAST:event_areaListMouseClicked

    private void areaNameTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_areaNameTextKeyReleased
        openArea.setName(areaNameText.getText());
        areaListModel.titleChange(openArea);
    }//GEN-LAST:event_areaNameTextKeyReleased

    private void areaDescriptionTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_areaDescriptionTextKeyReleased
        openArea.setDescription(areaDescriptionText.getText());
    }//GEN-LAST:event_areaDescriptionTextKeyReleased

    private void titleTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_titleTextKeyReleased
        openText.setName(titleText.getText());
        textListModel.titleChange(openText);
    }//GEN-LAST:event_titleTextKeyReleased

    private void sectionTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sectionTextKeyReleased
        openText.setText(sectionText.getText());
    }//GEN-LAST:event_sectionTextKeyReleased

    private void captionTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_captionTextKeyReleased
        openText.setCaption(captionText.getText());
    }//GEN-LAST:event_captionTextKeyReleased

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        textList.getSelectedValue().showTitle(jCheckBox1.isSelected());
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {                                                                                        
        // Compare Areas
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select file to compare.");
        chooser.setFileFilter(new FileNameExtensionFilter("Topo  Files", "tp"));
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {        
            try{
                Crag crag1 = new Crag(chooser.getSelectedFile());
                CragDiff cd = new CragDiff();
                cd.setDiffCrag(new DiffCrag(crag, crag1));
                cd.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                cd.setVisible(true);
                openCrag(crag);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }                                                                                      
    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        JFileChooser outChooser = new JFileChooser();
        outChooser.setDialogTitle("Output file");
        outChooser.setFileFilter(new FileNameExtensionFilter("XML Files", "xml"));
        outChooser.setCurrentDirectory(cragFile.getParentFile());
        if (outChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
            File f = outChooser.getSelectedFile();
            if(!f.getAbsolutePath().endsWith(".xml"))
                f = new File(f.getAbsolutePath() + ".xml");
            try{
                new CragXslt(crag).write(new FileOutputStream(f));
            } catch(FileNotFoundException | TransformerException ex){
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jMenuItem12ActionPerformed
                                   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Main m = new Main();
                if(args.length == 1){
                    File f = new File(args[0]);
                    if(f.exists())
                        m.openFile(f);
                    else
                        System.out.println("Error: File not found.");
                }                    
                m.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaDescriptionText;
    private javax.swing.JList<Area> areaList;
    private javax.swing.JTextField areaNameText;
    private javax.swing.JButton browseButton;
    private javax.swing.JTextField captionText;
    private javax.swing.JButton deleteAreaButton;
    private javax.swing.JButton deleteTextButton;
    private javax.swing.JButton editRoutesButton;
    private javax.swing.JTextField imageText;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton newAreaButton;
    private javax.swing.JButton newTextButton;
    private javax.swing.JButton removeTextImageButton;
    private javax.swing.JButton resetAreaButton;
    private javax.swing.JButton resetTextButton;
    private javax.swing.JTextArea sectionText;
    private javax.swing.JList<TextArea> textList;
    private javax.swing.JTextField titleText;
    // End of variables declaration//GEN-END:variables

}
