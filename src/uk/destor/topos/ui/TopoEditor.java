/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui;

import uk.destor.topos.data.TopoLine;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import javax.swing.JComponent;
import uk.destor.topos.data.TopoEntry;
import uk.destor.topos.data.TopoImage;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoEditor extends TopoEditorBase{
  
    public TopoEditor() {
        
    }

    @Override
    public void setTopoImage(TopoImage topoImage){
        super.setTopoImage(topoImage);        
        
        if(topoImage != null){
            topoImage.getTopoEntrys().forEach((te) -> {
                te.getTopoLine(topoImage.getImageName()).setParent(this, topoImage.getImgSize(), offset, scale);
                te.getTopoLine(topoImage.getImageName()).setEditable(false);
            });
        }
        deselectAllLines(); //Make sure the new lines are deselected
        repaint();
    }

    @Override
    protected void resized(double scale, Point offset){
        topoImage.getTopoEntrys().forEach((te) -> {
            te.getTopoLine(topoImage.getImageName()).setScale(scale);
            te.getTopoLine(topoImage.getImageName()).setOffset(offset);
        });
    }
    
    public void deselectAllLines(){
        if(topoImage != null)
            for(TopoEntry te : topoImage.getTopoEntrys())
                for(TopoLine tl : te.getTopoLines().values())
                    tl.setEditable(false);
        repaint();
    }
    public TopoLine newTopoLine(){
        if(topoImage == null)
            return null;
        deselectAllLines();
        TopoLine tl = new TopoLine(this, topoImage.getImgSize(), offset, scale);
        tl.setTopoImage(topoImage.getImageName());
        return tl;
    }

    @Override
    protected void paintLines(Graphics g){
        topoImage.getTopoEntrys().forEach((te) -> {
            te.getTopoLine(topoImage.getImageName()).draw(g);
        });
    }
}
