/*
 * Copyright (C) 2019 rob
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 *
 * @author rob
 */
public class ListModelImpl<E> implements ListModel<E>{
    private final List<E> list;
    private final Collection<ListDataListener> listDataListeners = new ArrayList<>();

    public ListModelImpl(List<E> list) {
        this.list = list;
    }

    public ListModelImpl(){
        list = new ArrayList<>();
    }
    
    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public E getElementAt(int i) {
        return list.get(i);
    }

    @Override
    public void addListDataListener(ListDataListener arg0) {
        listDataListeners.add(arg0);
    }
    @Override
    public void removeListDataListener(ListDataListener arg0) {
        listDataListeners.remove(arg0);
    }

    public void remove(E e) {
        int i = list.indexOf(e);
        list.remove(e);
        for (ListDataListener l : listDataListeners)
            l.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, i, i));
    }

    public void add(E e) {
        list.add(e);
        int i = list.indexOf(e);
        for (ListDataListener l : listDataListeners)
            l.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, i, i));
    }
    public void add(int i, E e) {
        list.add(i, e);
        for (ListDataListener l : listDataListeners)
            l.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, i, i));
    }
    public void move(int from, int to) {
        list.add(to, list.get(from));
        list.remove(to < from ? from + 1: from);
        for (ListDataListener l : listDataListeners)
            l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, to, from));
    }
    public void titleChange(E item){
        int i = list.indexOf(item);
        for (ListDataListener l : listDataListeners)
            l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, i, i));
    }
    public void clear(){
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }
}
