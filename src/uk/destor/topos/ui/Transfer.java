/*
 * Copyright (C) 2019 rob
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;

/**
 *
 * @author rob
 */
public class Transfer<T> extends TransferHandler {
        private final DataFlavor df;
        private final JList<T> jList;
        private final ListModelImpl<T> list;

        public Transfer(Class<T> clazz, JList<T> jList, ListModelImpl<T> list) throws ClassNotFoundException{
            super(null);
            df = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + ";class=" + clazz.getCanonicalName());
            this.jList = jList;
            this.list = list;
        }

        @Override
        public int getSourceActions(JComponent c) {
            return TransferHandler.MOVE;
        }

        @Override
        protected Transferable createTransferable(JComponent c) {
            return new Transferable() {
                @Override
                public DataFlavor[] getTransferDataFlavors() {
                    return new DataFlavor[]{df};
                }
                
                @Override
                public boolean isDataFlavorSupported(DataFlavor arg0) {
                    return arg0 == df;
                }
                
                @Override
                public Object getTransferData(DataFlavor arg0) throws UnsupportedFlavorException, IOException {
                    if(isDataFlavorSupported(arg0))
                        return jList.getSelectedIndex();
                    throw new UnsupportedFlavorException(arg0);
                }
            };
        }

        @Override
        public boolean canImport(TransferHandler.TransferSupport support) {
            return support.isDataFlavorSupported(df);
        }

        @Override
        @SuppressWarnings("unchecked")
        public boolean importData(TransferHandler.TransferSupport support) {
            if (!support.isDrop())
                return false;
            
            try {
                JList.DropLocation dl = (JList.DropLocation)support.getDropLocation();
                
                int index = dl.getIndex();
                int i = (Integer) support.getTransferable().getTransferData(df);
                
                list.move(i, index);
                jList.setSelectedIndex(index);
                return true;
            } catch (UnsupportedFlavorException | IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }
    }