/*
 * Copyright (C) 2019 rob
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.ui;

import java.awt.Component;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.transform.stream.StreamSource;
import uk.destor.topos.data.Crag;
import uk.destor.topos.output.xslt.CragXslt;

/**
 *
 * @author rob
 */
public class CreatePdf extends javax.swing.JDialog {
    /**
     * This is a helper for the ComboBoxes.
     */
    private class WithString {
        private final InputStream is;
        private final String s;

        public WithString(InputStream is, String s) {
            this.is = is;
            this.s = s;
        }

        @Override
        public String toString() {
            return s;
        }

        public String getS() {
            return s;
        }
        public InputStream getInputStream(){
            return is;
        }
        
        public void selected(){}
    }
    private class Exporter extends SwingWorker<Boolean, Void> {

        @Override
        protected Boolean doInBackground() throws Exception {
            JFileChooser outChooser = new JFileChooser();
            outChooser.setFileFilter(new FileNameExtensionFilter("PDF Files", "pdf"));
            outChooser.setSelectedFile(new File(crag.getName() + ".pdf"));
            if (outChooser.showSaveDialog(CreatePdf.this) != JFileChooser.APPROVE_OPTION){
                enableUI(true);
                return false;
            }
            
            File outFile = outChooser.getSelectedFile();
            if(!outFile.getPath().endsWith(".pdf")) outFile = new File(outFile.getAbsolutePath().concat(".pdf"));
            if(outFile.exists() &&
                    JOptionPane.showConfirmDialog(CreatePdf.this, "Overwrite file?", "File Exists", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION){
                enableUI(true);
                return false;
            }
            
            try {
                @SuppressWarnings("unchecked")
                WithString template = (WithString) templateListModel.getSelectedItem();
                
                File dir = Files.createTempDirectory("toposOut").toFile(), outTemp = new File(dir, "out.pdf");
                new CragXslt(crag).createPdf(new FileOutputStream(outTemp), new StreamSource(template.getInputStream()), dir);
                 
                if(!outFile.getParentFile().exists())
                    Files.createDirectories(outFile.getParentFile().toPath());
                if(outFile.exists())
                    outFile.delete();
                Files.copy(outTemp.toPath(), outFile.toPath());
                dir.delete();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(CreatePdf.this, "An error occured when creating the pdf file.\n" + ex.getLocalizedMessage());
                Logger.getLogger(CreatePdf.class.getName()).log(Level.SEVERE, null, ex);
                enableUI(true);
                return false;
            }
            
            if(JOptionPane.showConfirmDialog(CreatePdf.this,"Export finished. Would you like to view the output?", "Successful Export", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                try {
                    Desktop.getDesktop().open(outChooser.getSelectedFile());
                } catch (IOException ex) {
                    Logger.getLogger(CreatePdf.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            enableUI(true);
            return true;
        }
    }
    
    private final Crag crag;
    private final DefaultComboBoxModel<WithString> templateListModel = new DefaultComboBoxModel<>();

    private boolean beingCreated = true;
    
    /**
     * Creates new form createPDF
     */
    public CreatePdf(java.awt.Frame parent, boolean modal, Crag crag) {
        super(parent, modal);
        initComponents();
        getRootPane().setDefaultButton(jButton1);
        jProgressBar1.setVisible(false);
        
        this.crag = crag;

        templateListModel.addElement(new WithString(getClass().getResourceAsStream("/crag2fop.xsl"), "A4 Page"));
        templateListModel.addElement(new WithString(getClass().getResourceAsStream("/crag2fop-mobile.xsl"), "Mobile"));
        templateListModel.addElement(new WithString(null, "Browse..."){
            @Override
            public void selected() {
                JFileChooser chooser = new JFileChooser();
                chooser.setFileFilter(new FileNameExtensionFilter("XSLT Stylesheets", "xsl"));
                if(chooser.showOpenDialog(CreatePdf.this) == JFileChooser.APPROVE_OPTION){
                    try {
                        WithString add = new WithString(new FileInputStream(chooser.getSelectedFile()), chooser.getSelectedFile().getName());
                        templateListModel.insertElementAt(add, templateListModel.getSize()- 1);
                        jComboBox1.setSelectedItem(add);
                    } catch(FileNotFoundException ex){
                        Logger.getLogger(CreatePdf.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }      
            }
        });
        beingCreated = false;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jProgressBar1 = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Export PDF");

        jLabel1.setText("Template:");

        jComboBox1.setModel(templateListModel);
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jButton1.setMnemonic('x');
        jButton1.setText("Export");
        jButton1.setToolTipText("");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setMnemonic('c');
        jButton2.setText("Close");
        jButton2.setToolTipText("");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLayeredPane1.setLayer(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jComboBox1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, 0, 317, Short.MAX_VALUE))
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(33, Short.MAX_VALUE)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(34, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        templateListModel.getElementAt(jComboBox1.getSelectedIndex()).selected();
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    @SuppressWarnings("unchecked")
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    enableUI(false);
    jProgressBar1.setIndeterminate(true);
    new Exporter().execute();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void enableUI(boolean enable) {
        for (Component c : jLayeredPane1.getComponentsInLayer(JLayeredPane.DEFAULT_LAYER))
            c.setEnabled(enable);
        jProgressBar1.setVisible(!enable);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<WithString> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JProgressBar jProgressBar1;
    // End of variables declaration//GEN-END:variables

}
