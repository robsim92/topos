/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.output;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import uk.destor.topos.data.Area;
import uk.destor.topos.data.Crag;
import uk.destor.topos.data.TopoEntry;
import uk.destor.topos.data.TopoImage;
import uk.destor.topos.output.xslt.TopoEntryXslt;
import uk.destor.topos.output.xslt.TopoImageXslt;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class SvgOutput extends Output{
    private final Area area;
    private final TopoImage topoImage;
    private StreamSource xsltSource;
    
    public SvgOutput(Crag crag, Area area, TopoImage topoImage) {
        super(crag);
        this.area = area;
        this.topoImage = topoImage;
    }
    
    public void copyImageFile(File dir) throws IOException{
        ImageIO.write(processImage((BufferedImage) topoImage.getImage()),
                topoImage.getImageName().substring(topoImage.getImageName().lastIndexOf(".") + 1),
                new File(dir, "areas-" + area.getName() +"-" + topoImage.getImageName()));
    }

    @Override
    public void setTemplate(InputStream is) throws IOException {
        xsltSource = new StreamSource(is);
    }

    @Override
    public void createOutput(OutputStream os) throws IOException {
        try{
            TopoImageXslt tix = new TopoImageXslt(topoImage, area);
            int count = 1;
            for(TopoEntry te : area.getTopoEntries()){
                if(te.getTopoLines().containsKey(topoImage.getImageName()))
                    tix.addTopoEntry(new TopoEntryXslt(tix, te, count));
                count++;
            }
            
            PipedOutputStream pos = new PipedOutputStream();
            StreamSource source = new StreamSource(new PipedInputStream(pos));
            
            Thread t = new Thread(){
                @Override
                public void run(){
                    try{
                        StreamResult result = new StreamResult(os);
                        Transformer t = TransformerFactory.newInstance().newTransformer(xsltSource);
                        t.transform(source, result);
                    } catch(TransformerException ex){
                        Logger.getLogger(SvgOutput.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    synchronized (this){
                        notifyAll();
                    }
                }
            };
            t.start();
            
            tix.write(pos);
            pos.close();
            
            synchronized (t){
                t.wait();
            }
        } catch(TransformerException | InterruptedException ex){
            Logger.getLogger(SvgOutput.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) throws TransformerException, IOException {
        Crag c = new Crag(new File("/home/rob/Neilston-Topo-SG-Edit_RS_UUIDs.tp"));
        Area a = c.getAreas().get(1);
        for(TopoImage ti : a.getTopoImages()){
            SvgOutput output = new SvgOutput(c, a, ti);
            output.setTemplate(new FileInputStream("/home/rob/Code/fop-2.4/fop/topoImage2svg.xsl"));
            output.copyImageFile(new File("/home/rob/Code/fop-2.4/fop/"));
            output.createOutput(new FileOutputStream("/home/rob/Code/fop-2.4/fop/areas-" + a.getName() + "-" + ti.getImageName() + ".svg"));
        }
    }
}
