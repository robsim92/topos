/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.output;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import uk.destor.topos.data.Area;
import uk.destor.topos.data.Crag;
import uk.destor.topos.data.TextArea;
import uk.destor.topos.data.TopoEntry;
import uk.destor.topos.data.TopoImage;
import uk.destor.topos.data.TopoLine;
import uk.destor.topos.data.parameters.Parameter;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class LatexOutput extends Output{
    private enum CragTag{
        NAME("cragName"){
            @Override
            public String getValue(Crag c) {
                return c.getName();
            }
        },
        TEXT("authors"){
            @Override
            public String getValue(Crag c) {
                return c.getTopoAuthors();
            }
        },
        LICENSE("license"){
            @Override
            public String getValue(Crag c) {
                return c.getLicense();
            }
        },
        COMMENT("%"){
            @Override
            public String getValue(Crag c) {
                return "";
            }
        };
        private final String tag;

        private CragTag(String tag) {
            this.tag = tag;
        }
        public String getTag() {
            return "<<" + tag + ">>";
        }
        public String output(String template, Crag c){
            String value = getValue(c);
            return template.replaceAll(getTag(), value == null ? "" : value.replaceAll(Matcher.quoteReplacement("\\"), Matcher.quoteReplacement("\\\\")));
        }
        protected abstract String getValue(Crag c);
    }
    private enum TextTag{
        NAME("textName"){
            @Override
            public String getValue(TextArea ta) {
                return ta.showTitle() ? ta.getName() : "";
            }
        },
        TEXT("text"){
            @Override
            public String getValue(TextArea ta) {
                return ta.getText();
            }
        },
        IMAGE("textImage"){
            @Override
            public String getValue(TextArea ta) {
                if(ta.hasImage())
                    return imageFile("TextAreas/" + ta.getName() + "/" + ta.getImageName());
                else
                    return null;
            }
        },
        CAPTION("textCaption"){
            @Override
            public String getValue(TextArea ta) {
                return ta.getCaption();
            }
        },
        COMMENT("%"){
            @Override
            public String getValue(TextArea ta) {
                return "";
            }
        };
        private final String tag;

        private TextTag(String tag) {
            this.tag = tag;
        }
        public String getTag() {
            return "<<" + tag + ">>";
        }
        public String output(String template, TextArea ta){
            String value = getValue(ta);
            return template.replaceAll(getTag(), value == null ? "" : value.replaceAll(Matcher.quoteReplacement("\\"), Matcher.quoteReplacement("\\\\")));
        }
        protected abstract String getValue(TextArea ta);
    }
    private enum AreaTag{
        NAME("areaName"){
            @Override
            public String getValue(Area a) {
                return a.getName();
            }
        },
        DESCRIPTION("areaDescription"){
            @Override
            public String getValue(Area a) {
                return a.getDescription();
            }
        },
        COMMENT("%"){
            @Override
            public String getValue(Area a) {
                return "";
            }
        };
        private final String tag;

        private AreaTag(String tag) {
            this.tag = tag;
        }
        public String getTag() {
            return "<<" + tag + ">>";
        }
        public String output(String template, Area a){
            String value = getValue(a);
            return template.replaceAll(getTag(), value == null ? "" : value.replaceAll(Matcher.quoteReplacement("\\"), Matcher.quoteReplacement("\\\\")));
        }
        protected abstract String getValue(Area a);
    }
    private enum ImageTag{
        IMAGE("image"){
            @Override
            public String getValue(Area a, TopoImage ti) {
                return imageFile("Areas/" + a.getName() + "/" + ti.getImageName());
            }
        },
        COMMENT("%"){
            @Override
            public String getValue(Area a, TopoImage ti) {
                return "";
            }
        };
        private final String tag;

        private ImageTag(String tag) {
            this.tag = tag;
        }
        public String getTag() {
            return "<<" + tag + ">>";
        }
        public String output(String template, Area a, TopoImage ti){
            String value = getValue(a, ti);
            return template.replaceAll(getTag(), value == null ? "" : value.replaceAll(Matcher.quoteReplacement("\\"), Matcher.quoteReplacement("\\\\")));
        }
        protected abstract String getValue(Area a, TopoImage ti);
    }
    private enum TopoTag {
        NAME("name"){
            @Override
            public String getValue(TopoEntry te) {
                return te.getName();
            }
        },
        NUMBER("number"){
            @Override
            public String getValue(TopoEntry te) {
                return te.getNumber();
            }
        },
        DESCRIPTION("description"){
            @Override
            public String getValue(TopoEntry te) {
                return te.getDescription();
            }
        },
        GRADE("grade"){
            @Override
            public String getValue(TopoEntry te) {
                return te.getGrade();
            }
        },
        FA("fa"){
            @Override
            public String getValue(TopoEntry te) {
                return te.getFirstAscensionist();
            }
        },
        FA_YEAR("faYear"){
            @Override
            public String getValue(TopoEntry te) {
                return te.getYearOfFirstAscent();
            }
        },
        COMMENT("%"){
            @Override
            public String getValue(TopoEntry te) {
                return "";
            }
        };
        private final String tag;

        private TopoTag(String tag) {
            this.tag = tag;
        }
        public String getTag() {
            return "<<" + tag + ">>";
        }
        public String output(String template, TopoEntry te){
            String value = getValue(te);
            return template.replaceAll(getTag(), value == null ? "" : value.replaceAll(Matcher.quoteReplacement("\\"), Matcher.quoteReplacement("\\\\")));
        }
        protected abstract String getValue(TopoEntry te);
    }
    private enum TopoLineTag {
        LINE("line"){
            @Override
            public String getValue(TopoLine tl) {
                if(tl == null)
                    return null;
                return tl.assymptoteString();
            }
        },
        LINE_NO_START("lineNoStartEnd"){
            @Override
            public String getValue(TopoLine tl) {
                if(tl == null)
                    return null;
                return tl.assymptoteString(false, false);
            }
        },
        STRART("start"){
            @Override
            public String getValue(TopoLine tl) {
                if(tl == null || tl.isEmpty() || !tl.hasImage())
                    return null;
                String s = tl.startPoint();
                return s.substring(1, s.length()-1);
            }
        },
        END("end"){
            @Override
            public String getValue(TopoLine tl) {
                if(tl == null || tl.isEmpty() || !tl.hasImage())
                    return null;
                String s = tl.endPoint();
                return s.substring(1, s.length()-1);
            }
        },
        COMMENT("%"){
            @Override
            public String getValue(TopoLine tl) {
                return "";
            }
        };
        private final String tag;

        private TopoLineTag(String tag) {
            this.tag = tag;
        }
        public String getTag() {
            return "<<" + tag + ">>";
        }
        public String output(String template, TopoLine tl){
            String value = getValue(tl);
            return template.replaceAll(getTag(), value == null ? "" : value.replaceAll(Matcher.quoteReplacement("\\"), Matcher.quoteReplacement("\\\\")));
        }
        protected abstract String getValue(TopoLine tl);
    }
    private enum PaperSize{
//        A4("a4paper", 5000, 7020),
//        A6("a6paper", 2500, 3510);
        A4("a4paper", 1666, 2340),
        A6("a6paper", 1250, 1755);
        private final String name;
        private final int maxWidth, maxHeight;

        private PaperSize(String name, int maxWidth, int maxHeight) {
            this.name = name;
            this.maxWidth = maxWidth;
            this.maxHeight = maxHeight;
        }

        public String getName() {
            return name;
        }
        public BufferedImage resizeImage(BufferedImage in){
            double scale = Math.min((double)maxWidth/(double)in.getWidth(null), (double)maxHeight/(double)in.getHeight(null));
            if(scale < 1){
                BufferedImage newImage = new BufferedImage((int) (in.getWidth()*scale), (int) (in.getHeight()*scale), BufferedImage.TYPE_INT_RGB);
                Graphics2D g = newImage.createGraphics();
                Image i = in.getScaledInstance(newImage.getWidth(), newImage.getHeight(), Image.SCALE_SMOOTH);
                g.drawImage(i, 0, 0, null);
                g.dispose();
                return newImage;
            }else
                return in;
        }
    }

    private String template = null;

    public LatexOutput(Crag crag) {
        super(crag);
    }

    @Override
    public BufferedImage processImage(BufferedImage i) {
        Matcher m = Pattern.compile("\\D\\dpaper").matcher(template);
        if(m.find()){
            String paper = m.group();
            for (PaperSize ps : PaperSize.values())
                if(ps.getName().equals(paper))
                    return ps.resizeImage(i);
        }
        return i;
    }
    
    @Override
    public void setTemplate(InputStream is) throws IOException{
        try(BufferedReader br = new BufferedReader(new InputStreamReader(is))){
            StringBuilder sb = new StringBuilder();
            for(String in = br.readLine(); in != null; in = br.readLine())
                sb.append(in).append("\r\n");
            template = sb.toString();
        }    
    }

    @Override
    public void createOutput(OutputStream os) throws IOException{
       String out = cragToLatex(crag, template);
       out = split("textArea", out, (sb, s) -> {
            crag.getTextAreas().forEach((ta) ->{
                sb.append(textToLatex(ta, s));
            });
        });
        
        try(BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os))){
            bw.append(split("area", out, (sb, s) -> {
                crag.getAreas().forEach((a) ->{
                    sb.append(areaToLatex(a, s));
                });
            }));
            bw.flush();
        } 
    }
    private String split(String tag, String s, BiConsumer<StringBuilder, String> bc){
        StringBuilder sb = new StringBuilder();
        for (String s1 : s.split("<<" + tag + ">>")) {
            String[] ss = s1.split("<<!" + tag +">>");
            if (ss.length == 1)
                sb.append(ss[0]);
            else{
                bc.accept(sb, ss[0]);
                for(int q = 1; q < ss.length; q++)
                    sb.append(ss[q]);
            }
        }
        
        return sb.toString();
    }
    private String cragToLatex(Crag c, String template){
        String crag = template;
        for (CragTag t : CragTag.values())
            crag = t.output(crag, c);
        return crag;
    }
    private String textToLatex(TextArea ta, String textTemplate){
        String text = textTemplate;
        for (TextTag t : TextTag.values())
            text = t.output(text, ta);
        return text;
    }
    private String areaToLatex(Area a, String areaTemplate){
        String lastImage = null;
        Map<String, List<TopoEntry>> m = new LinkedHashMap<>();
        for (TopoEntry te : a.getTopoEntries()) {
            if(te.getTopoLines().isEmpty()){
                if(lastImage == null){
                    if(!m.containsKey("")) 
                        m.put("", new ArrayList<>());
                    m.get("").add(te);
                }else
                    m.get(lastImage).add(te);
            }else{
                for (TopoLine tl : te.getTopoLines().values()) {
                    if(!m.containsKey(tl.getTopoImage()))
                        m.put(lastImage = tl.getTopoImage(), new ArrayList<>());
                    m.get(tl.getTopoImage()).add(te);
                }
            }
        }
        
        for (int i = 0; i < a.getTopoEntries().size(); i++)
            a.getTopoEntries().get(i).setNumber(i+1);
        
        String area = split("img", areaTemplate, (sb, s) -> {
            for (Map.Entry<String, List<TopoEntry>> e : m.entrySet())
                sb.append(imgToLatex(a, a.getTopoImage(e.getKey()), e.getValue(), s));
        });

        area = split("routes", area, (sb, s) -> {
            a.getTopoEntries().forEach((t) -> {
                sb.append(topoToLatex(t, s));
            });
        });
                
        for (AreaTag t : AreaTag.values())
            area = t.output(area, a);
        return area;
    }
    private String imgToLatex(Area a, TopoImage ti, List<TopoEntry> tes, String imgTemplate){
        String img = split("routes", imgTemplate, (sb, s) -> {
            tes.forEach((t) -> {
                sb.append(topoLineToLatex(t.getTopoLine(ti.getImageName()), t, s));
            });
        });
        
        for (ImageTag t : ImageTag.values())
            img = t.output(img, a, ti);

        return img;
    }
    private String topoToLatex(TopoEntry te, String routeTemplate){
        String route = routeTemplate;
        for (TopoTag t : TopoTag.values())
            route = t.output(route, te);
        
        if(te.isProject())
            route = route.replaceAll("<<Route-Project>>", "Project").replaceAll("<<route-project>>", "project");
        else
            route = route.replaceAll("<<Route-Project>>", "Route").replaceAll("<<route-project>>", "route");

        return route;
    }
    private String topoLineToLatex(TopoLine tl, TopoEntry te, String lineTempate){
        String route = lineTempate;
        for (TopoLineTag t : TopoLineTag.values())
            route = t.output(route, tl);
        route = TopoTag.NUMBER.output(route, te);
        
        if(te.isProject())
            route = route.replaceAll("<<Route-Project>>", "Project").replaceAll("<<route-project>>", "project");
        else
            route = route.replaceAll("<<Route-Project>>", "Route").replaceAll("<<route-project>>", "route");

        return route;
    }
    
    private static String imageFile(String file){
        int i = file.lastIndexOf('.');
        if(i == -1) return file;
        else return '"' + file.substring(0, i) + '"' + file.substring(i);
    }
}
