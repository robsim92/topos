/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.output;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import uk.destor.topos.data.Crag;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public abstract class Output {
    protected final Crag crag;

    public Output(Crag crag) {
        this.crag = crag;
    }
    
    public BufferedImage processImage(BufferedImage i){
        return i;
    }
    
    public abstract void setTemplate(InputStream is) throws IOException;
    public abstract void createOutput(OutputStream os) throws IOException;
}
