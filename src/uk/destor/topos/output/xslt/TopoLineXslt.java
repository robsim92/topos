/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.output.xslt;

import java.awt.Image;
import java.awt.geom.Point2D;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.Area;
import uk.destor.topos.data.TopoLine;
import uk.destor.topos.data.XmlParser;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoLineXslt extends XmlParser{
    private final TopoLine topoLine;
    private final TopoImageXslt topoImageXslt;
    private final int count;

    public TopoLineXslt(TopoLine topoLine, TopoImageXslt topoImageXslt, int count) {
        super("TopoLine");
        this.topoLine = topoLine;
        this.topoImageXslt = topoImageXslt;
        this.count = count;
    }

    @Override
    protected Attributes elementAttributes() {
        if(topoLine.isEmpty()) return super.elementAttributes();
        
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "count", "count", "CDATA", String.valueOf(count));
        TopoLine.TopoPoint tp = topoLine.getLinePoints().get(0);
        ai.addAttribute(namespaceUri(), "x1", "x1", "CDATA", String.valueOf((int)(tp.getPosition().x*100)));
        ai.addAttribute(namespaceUri(), "y1", "y1", "CDATA", String.valueOf((int)(tp.getPosition().y*100)));
        tp = topoLine.getLinePoints().get(topoLine.getLinePoints().size() - 1);
        ai.addAttribute(namespaceUri(), "x2", "x2", "CDATA", String.valueOf((int)(tp.getPosition().x*100)));
        ai.addAttribute(namespaceUri(), "y2", "y2", "CDATA", String.valueOf((int)(tp.getPosition().y*100)));
        ai.addAttribute(namespaceUri(), "line", "line", "CDATA", generateLine());
        Point2D.Double d = topoLine.getNearestPointToTag();
        ai.addAttribute(namespaceUri(), "xTag1", "xTag1", "CDATA", String.valueOf((int)(d.x*100)));
        ai.addAttribute(namespaceUri(), "yTag1", "yTag1", "CDATA", String.valueOf((int)(d.y*100)));
        ai.addAttribute(namespaceUri(), "xTag", "xTag", "CDATA", String.valueOf((int)(topoLine.getTag().x*100)));
        ai.addAttribute(namespaceUri(), "yTag", "yTag", "CDATA", String.valueOf((int)(topoLine.getTag().y*100)));
        return ai;
    }
    private String generateLine(){
        String s = "";
        for(TopoLine.TopoPoint tp : topoLine.getLinePoints()){
            if(tp.getControl1() == null)
                s = pointToString(tp.getControl2());
            else{
                s += ", " + pointToString(tp.getControl1()) + ", " + pointToString(tp.getPosition());
                if(tp.getControl2() != null)
                    s += ", " + pointToString(tp.getControl2());
            }
        }
        return s;
    }
    private String pointToString(Point2D p){
        return String.valueOf((int)(p.getX()*100)) + " " + String.valueOf((int)(p.getY()*100));
    }
}
