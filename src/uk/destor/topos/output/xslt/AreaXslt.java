/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.output.xslt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.Area;
import uk.destor.topos.data.TopoEntry;
import uk.destor.topos.data.TopoImage;
import uk.destor.topos.data.XmlParser;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class AreaXslt extends XmlParser{
    private final Area area;
    private final List<TopoImageXslt> topoImages = new ArrayList<>();

    public AreaXslt(Area area){
        super("Area");
        this.area = area;
        
        int count = 1;
        Iterator<TopoImage> topoImageIterator = area.getTopoImages().listIterator();
        TopoImage currentImage = topoImageIterator.hasNext()?topoImageIterator.next():null;
        TopoImageXslt topoImageXslt = new TopoImageXslt(currentImage, area);
        for(TopoEntry te : area.getTopoEntries()){
            if(!te.getTopoLines().isEmpty() && !te.getTopoLines().containsKey(currentImage.getImageName())){
                topoImages.add(topoImageXslt);
                currentImage = topoImageIterator.hasNext()?topoImageIterator.next():null;
                topoImageXslt = new TopoImageXslt(currentImage, area);
            }
            topoImageXslt.addTopoEntry(new TopoEntryXslt(topoImageXslt, te, count++));
        }
        if(!topoImageXslt.isEmpty())
            topoImages.add(topoImageXslt);
    }
    
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "name", "name", "CDATA", area.getName());
        return ai;
    }

    @Override
    protected void outputChildren(ContentHandler contentHandler) throws SAXException{
        createXmlElements(contentHandler, topoImages);
        createStringElement(contentHandler, "Description", area.getDescription());
    }
}
