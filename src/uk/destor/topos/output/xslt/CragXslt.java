/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.output.xslt;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.xmlgraphics.util.MimeConstants;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.Area;
import uk.destor.topos.data.Crag;
import uk.destor.topos.data.TextArea;
import uk.destor.topos.data.TopoImage;
import uk.destor.topos.data.XmlParser;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class CragXslt extends XmlParser{
    private final Crag crag;
    private final List<AreaXslt> areaXslts = new ArrayList<>();
    public CragXslt(Crag crag){
        super("Crag");
        this.crag = crag;
        
        for(Area a : crag.getAreas())
            areaXslts.add(new AreaXslt(a));
    }
    
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "name", "name", "CDATA", crag.getName());
        ai.addAttribute(namespaceUri(), "topoAuthors", "topoAuthors", "CDATA", crag.getTopoAuthors());
        ai.addAttribute(namespaceUri(), "date", "date", "CDATA", new SimpleDateFormat("dd MMMM yyyy").format(new Date()));
        return ai;
    }

    @Override
    protected void outputChildren(ContentHandler contentHandler) throws SAXException{
        createXmlElements(contentHandler, areaXslts);
        createXmlElements(contentHandler, crag.getTextAreas());
        createStringElement(contentHandler, "License", crag.getLicense());
    }
    
    public void createPdf(OutputStream os, StreamSource xslt, File outputDir) throws IOException, FOPException, TransformerException{
        if(!outputDir.isDirectory()) throw new RuntimeException("outputDir must be a directory.");
        
        SAXSource source = getSAXSource();
        
        for(TextArea ta : crag.getTextAreas())
            if(ta.hasImage())
                copyImageFile(outputDir, ta.getImage(), ta.getImageName(), "");
        for(Area a : crag.getAreas())
            for(TopoImage ti : a.getTopoImages())
                    copyImageFile(outputDir, ti.getImage(), ti.getImageName(), "areas-" + a.getName() + "-");

        FopFactory fopFactory = FopFactory.newInstance(outputDir.toURI());
        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, os);

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(xslt);

        transformer.transform(source, new SAXResult(fop.getDefaultHandler()));
    }
    private  void copyImageFile(File dir, Image i, String name, String prefix) throws IOException{
        ImageIO.write((BufferedImage) i, name.substring(name.lastIndexOf(".") + 1), new File(dir, prefix + name));
    }
}
