/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.output.xslt;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.Area;
import uk.destor.topos.data.TopoEntry;
import uk.destor.topos.data.TopoLine;
import uk.destor.topos.data.XmlParser;
import uk.destor.topos.data.parameters.Parameter;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoEntryXslt extends XmlParser{
    private final TopoEntry topoEntry;
    private final int count;
    
    public TopoEntryXslt(TopoImageXslt topoImageXslt, TopoEntry topoEntry, int count) {
        super("Route");
        this.topoEntry = topoEntry;
        this.count = count;
    }
    
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();        
        ai.addAttribute(namespaceUri(), "name", "name", "CDATA", topoEntry.getName());
        addAttribute(ai, "grade", topoEntry.getGrade());
        ai.addAttribute(namespaceUri(), "project", "project", "CDATA", Boolean.toString(topoEntry.isProject()));
        addAttribute(ai, "fa", topoEntry.getFirstAscensionist());
        addAttribute(ai, "faYear", topoEntry.getYearOfFirstAscent());
        ai.addAttribute(namespaceUri(), "count", "count", "CDATA", String.valueOf(count));
        return ai;
    }
    private void addAttribute(AttributesImpl ai, String name, String value){
        ai.addAttribute(namespaceUri(), name, name, "CDATA", value==null?"":value);
    }
    @Override
    protected void outputChildren(ContentHandler contentHandler) throws SAXException {
        createStringElement(contentHandler, "Description", topoEntry.getDescription());
    }
}
