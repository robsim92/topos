/*
 * Copyright (C) 2019 Rob Sim <robsim92 at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.destor.topos.output.xslt;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.destor.topos.data.Area;
import uk.destor.topos.data.TopoEntry;
import uk.destor.topos.data.TopoImage;
import uk.destor.topos.data.TopoLine;
import uk.destor.topos.data.XmlParser;

/**
 *
 * @author Rob Sim <robsim92 at gmail.com>
 */
public class TopoImageXslt extends XmlParser{
    private final TopoImage topoImage;
    private final List<TopoEntryXslt> topoEntrys = new ArrayList<>();
    private final int width, height;
    private final Area area;

    public TopoImageXslt(TopoImage topoImage, Area area) {
        super("TopoImage");
        this.topoImage = topoImage;
        
        if(topoImage != null){
              width = topoImage.getImage().getWidth(null);
            height = topoImage.getImage().getHeight(null);
        }else
            width = height = 0;
            
        this.area = area;
    }

    public void addTopoEntry(TopoEntryXslt tex){
        topoEntrys.add(tex);
    }
    public boolean isEmpty(){
        return topoEntrys.isEmpty();
    }
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    public String getImageName(TopoLine tl){
        return "areas-" + area.getName() + "-" + tl.getTopoImage();
    }
    
    @Override
    protected Attributes elementAttributes() {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(namespaceUri(), "image", "image", "CDATA", topoImage==null?"":"areas-" + area.getName() + "-" + topoImage.getImageName());
        ai.addAttribute(namespaceUri(), "width", "width", "CDATA", String.valueOf(width));
        ai.addAttribute(namespaceUri(), "height", "height", "CDATA", String.valueOf(height));
        return ai;
    }

    @Override
    protected void outputChildren(ContentHandler contentHandler) throws SAXException {
        createXmlElements(contentHandler, topoEntrys);
        if(topoImage != null){
            int count = 1;
            for(TopoEntry te : area.getTopoEntries()){
                for(TopoLine tl : te.getTopoLines().values())
                    if(tl.getTopoImage().equals(topoImage.getImageName()))
                        new TopoLineXslt(tl, this, count).createXmlElement(contentHandler);
                count++;
            }
        }
    }
}
