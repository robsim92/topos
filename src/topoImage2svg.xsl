<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
    <xsl:template match="TopoImage">
        <svg:svg width="{@width}" height="{@height}">    
            <svg:image xlink:href="{@image}" width="100%" height="100%"/>
            <xsl:apply-templates select="Route"/>
        </svg:svg>
    </xsl:template>
    <xsl:template match="Route">
        <xsl:apply-templates select="TopoLine"/>
    </xsl:template>  
    <xsl:template match="TopoLine">   
        <xsl:if test="@image=../../@image">
            <xsl:variable name="colourMod" select="../@count mod 9"/>
            <xsl:variable name="colour">
                <xsl:choose>
                    <xsl:when test="$colourMod=1">red</xsl:when>
                    <xsl:when test="$colourMod=2">blue</xsl:when>
                    <xsl:when test="$colourMod=3">yellow</xsl:when>
                    <xsl:when test="$colourMod=4">green</xsl:when>
                    <xsl:when test="$colourMod=5">magenta</xsl:when>
                    <xsl:when test="$colourMod=6">purple</xsl:when>
                    <xsl:when test="$colourMod=7">lime</xsl:when>
                    <xsl:when test="$colourMod=8">pink</xsl:when>
                    <xsl:when test="$colourMod=0">orange</xsl:when>
                </xsl:choose>
            </xsl:variable>
            <svg:path d="M {@x1} {@y1} C {@line}" stroke="{$colour}" stroke-width="3" fill="none"/>
            <svg:circle style="fill:white;stroke:{$colour}" stroke-width="2" cx="{@x1}" cy="{@y1}" r="12"/>
            <svg:circle style="fill:{$colour};stroke:{$colour}" stroke-width="2" cx="{@x2}" cy="{@y2}" r="2"/>
            <svg:text x="{@x1}" y="{@y1}" text-anchor="middle" font-size="10pt" dominant-baseline="middle" stroke="black"><xsl:value-of select="../@count"/></svg:text>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>