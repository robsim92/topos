\documentclass[a4paper,10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{parskip}
\usepackage[UKenglish]{babel}
\usepackage[section]{placeins}
\usepackage[T1]{fontenc}
 
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}
\urlstyle{same}

\newcommand{\pn}{Topo Builder}
\newcommand{\pv}{Beta 2}
\newcommand{\mainUrl}{\url{https://bitbucket.org/robsim92/topos/}}
\newcommand{\downloadUrl}{\url{https://bitbucket.org/robsim92/topos/downloads/}}

\let\origsection\section
\renewcommand\section[2][\relax]{%
  \vspace{-\parskip}
  \ifx\relax#1\origsection{#2}\else\origsection[#1]{#2}\fi%
  \vspace{-\parskip}
}

\let\origsubsection\subsection
\renewcommand\subsection[2][\relax]{%
  \vspace{-\parskip}
  \ifx\relax#1\origsubsection{#2}\else\origsubsection[#1]{#2}\fi%
  \vspace{-\parskip}
}

\let\origsubsubsection\subsubsection
\renewcommand\subsubsection[2][\relax]{%
  \vspace{-\parskip}
  \ifx\relax#1\origsubsubsection{#2}\else\origsubsubsection[#1]{#2}\fi%
  \vspace{-\parskip}
}


\title{\pn\ User Guide\\ {\large \pv}}
\author{Robert Sim}

\begin{document}

\maketitle

\begin{figure}[h]
  \centering
    \includegraphics[width=0.8\textwidth]{Cover}
\end{figure}

\vfill
\noindent
Copyright 2019 Robert Sim\\
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit \url{http://creativecommons.org/licenses/by-sa/4.0}.
\newpage

\section{Introduction}
\pn\ is a simple program for recording topos for climbing routes/bouldering problems.
It consists of an interface for the entry of route/problem information and allows a topo line to be drawn on a picture of the crag/boulder.
A topo document or guidebook can be produced from this.

\section{Installation}
There is no need to install \pn\ on your computer.
Once downloaded from \downloadUrl\ extract the contents of the file to your preferred installation location (if desired, this could be on a pen drive for use on several computers).
\pn\ can then be ran by double clicking the \emph{\pn.jar} file. 

If it does not run please ensure that you have the latest version of Java (\url{https://www.java.com/download/}) installed on your computer.

\section{Creating a New Topo}
\subsection{Describing the Crag}
The name of the crag and the author of the topos document can be set from the \emph{Crag Info} menu.

Sections of text consisting of a title, some text and an optional picture can be added to the start of the Topo.
This is referred to as a \emph{text area} hereinafter.
This allows an introduction, access information or any other useful crag information to be added to the topo.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.42\textwidth]{TextArea}
    \quad
    \includegraphics[width=0.45\textwidth]{TextArea_Output}
    \caption{Crag description tab and the corresponding output.}
\end{figure}

\noindent
To add this, select the \emph{Crag Description} tab.
Once the text has been added click \emph{Update} to save the changes.
All text sections are located at the front of the topo document and will be ordered as they are in the list to the right of the window.
This order can be changed by dragging titles in the list.

\subsection{Adding Areas}
Areas represent individual areas of the crag and are found in the \emph{Routes} tab at the bottom left of the window.

Each area must contain a name, with an optional description, several topo images and the routes.
If any of these items are missing, this item will not be shown in the output.
This can be useful for a small crag where descriptions are not required. However, a larger one may have several and each area may have text describing access to the area, area specific notes, etc.
Examples are given in Figures \ref{img:Area fig 1} and \ref{Area fig 2}.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.45\textwidth]{AreaDesc1}\quad
    \includegraphics[width=0.45\textwidth]{AreaDesc1_Output}
    \caption{Area without a description.}
    \label{img:Area fig 1}
\end{figure}

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.45\textwidth]{AreaDesc2}\quad
    \includegraphics[width=0.45\textwidth]{AreaDesc2_Output}
    \caption{Area with a description. The first image is shown on the top of the next page.}
    \label{Area fig 2}
\end{figure}

The name, description and ordering of areas can be changed in the same manner as the crag descriptions.

Select the \emph{Edit Routes} to add/edit routes and images to the area.

\FloatBarrier
\subsection{Adding Topo Images}
Before adding routes at least one topo image should added.
This is the image that the lines of the routes will be displayed on.

%TODO: Maybe an image here?
Topo images can be added, replaced and deleted using the buttons to the bottom right of the window.
Select the thumbnail of an image to display the image and the routes on this image.

\subsection{Adding Routes}
The name, grade, description, etc of a route can be added using the inputs in the top left of the window.
New routes can be added to the end of the routes list by selecting \emph{New}, filling in the relevant details and climbing \emph{Update/Add}.
Routes are listed beneath their area in the topo document and are ordered as per the routes list in the editor.
The order can be changed by dragging the route name in the list.
This is shown in Figure \ref{Routes}

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.75\textwidth]{Routes}
    \caption{Routes editor.}
    \label{Routes}
\end{figure}

To add a route to the topo image click the start and finish points of the route to draw a straight line between them.
Click along the line to add additional points.
These points and the control points can be dragged to alter the route.
Right click a point to delete it.

\begin{figure}[!htb]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{TopoLine1}
        \caption{Selecting the start and finish.}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{TopoLine2}
        \caption{Adding and moving points.}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{TopoLine3}
        \caption{Moving control points.}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{TopoLine4}
        \caption{Right click to delete points.}
    \end{subfigure}
    \quad
    \caption{Adding a topo line to the image.}
\end{figure}

\FloatBarrier
\subsection{Exporting to PDF}
Currently, the topos can be exported to PDF or LaTeX files.
PDF files allow the creation of topo documents that can be easily shared or printed.
This is the option that the majority of users will use.

\subsubsection{Template}
The template to use when producing the pdf file.
One of the internal templates may be selected or an external one may be selected.
The internal templates can be used to produce an A4 portrait document for use on computer screens or printing or a smaller pdf that is designed to be viewed on a smartphone or tablet.
For each format there is a template using red for routes and yellow for projects and another that uses multiple colours and a dashed line for projects.
These are shown in Figure \ref{img:templates}.

Alternatively, a custom template can be used.
This is covered in Section \ref{sec:LaTeXOut} and requires the use of an external LaTeX installation.

% TODO: Redo inages with grades
\begin{figure}[!htb]
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{out1}
        \caption{Default.}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{out2}
        \caption{Default - Coloured Lines.}
    \end{subfigure}
\end{figure}
    
\begin{figure}[!htb]\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{out3}
        \caption{Mobile Optimised.}
    \end{subfigure}
    \hspace{0.1\textwidth}
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{out4}
        \caption{Mobile Optimised - Coloured Lines.}
    \end{subfigure}
    \quad
    \caption{Internal Templates (without grades added to the problems).}
    \label{img:templates}
\end{figure}

\FloatBarrier
\subsubsection{pdflatex File}
As LaTeX is used to generate the pdf file, the path to the pdflatex program must be available.
Microsoft Windows users can use the included version of LaTeX if using one of the included templates.
Therefore, most users are not expected to need to change this.

Users of other operating systems should install an external LaTeX package separately and select the installed pdflatex file.

To reduce the file size, the included LaTeX package contains only the components required to compile the included templates.
Therefore, if an external template is to be used an external LaTeX installation is required regardless of the operating system.

\subsubsection{Reduce Image Size}
If the \emph{Reduce Image Size} box is ticked, large topo images will be resized before creating the pdf file.
This reduces the file size of the final pdf.

\subsubsection{LaTeX}
LaTeX files allow topos to be easily converted into different formats or edited before being compiling into a PDF (or other format).

This option generates a raw LaTeX file for further editing and/or conversion to additional formats.
To produce a LaTeX file an external template must be used.
More details on this is presented in Section \ref{sec:LaTeXOut}.

\section{Topo Tips}
\subsection{Photos}
When taking photos for topos, the best results are generally achieved with the sun behind the camera (shining onto the rock).
Avoid large areas of shadow if possible.
If this is not possible, the brightness and contrast can be adjusted to compensate.
If using the camera on a smartphone, this can normally be done in the camera app.
Otherwise, you can use your favourite photo editing software to make the tweaks before importing the photos into \pn. 

Photos should also be cropped before being imported into \pn.
This ensures that superfluous sky and ground is not included in the final topo.

\subsection{Copyright}
In general the copyright of content, such as text or images, reside with the the author/creator of that work.
This has implications on the topo/guidebook creators in terms of how the copyright of the  new topo/guidebook is protected (or if it's protected) and the responsibility of ensuring that the new topo/guidebook does not infringe the copyright of others.

When adding routes to a topo, the following information is generally not subject to copyright and can therefore be included within a topo/guidebook without needing to seek permission.

\begin{itemize}
 \item Names
 \item Grades
 \item Height
 \item First Ascensionest 
 \item Date of First Ascent
 \item The physical line of the route on the rock
\end{itemize}

Route descriptions, photos/images, and the lines on topo images are subject to copyright and should not be included in topos/guidebooks within the copyright holder's permission.
It is however possible to re-create this information yourself. 
This can be achieved through writing a new description of the route (you can use previous descriptions as a reference), taking new pictures, etc.
It is worth noting that the copyright for short simplistic descriptions, such as \emph{climb the crack}, are unlikely to be enforceable.

If a work does not contain copyright or licence information this does not mean that the information can be freely used/copied.
In fact, the default position is that the author/creator has not granted any permissions to use their work, i.e. you may not use the work at all (including reading it or using it to find routes when climbing).
Therefore, it is recommended to explicitly include copyright information and a license in your topos/guidebooks, even if it will be released in to the public domain.
More information on open/free licenses that can be used can be found at \url{https://creativecommons.org/share-your-work/}.

Propriety licenses should discussed with an appropriately qualified person (e.g. copyright lawyer).

It is proposed to add the ability to include a copyright/license to later versions of this software.

Please note that the above does not constitute legal advice and an appropriately qualified person should be consulted to ensure compliance with the relevant copyright laws.

\subsection{Drawings Lines}
To get smooth corners, it is recommended that the points on the topo line are placed in the straighter parts of the route and the control points used to achieve a smooth corner.
If the points on the line are located on the corner, it will result in a sharp corner that may look out of place.

\section{LaTeX Template Formatting}
\label{sec:LaTeXOut}
Templates are used to create the output files.
This allows easy customisation of the output files and allows multiple output formats to be created.
This section outlines how to create custom templates.
In should be noted that while the default output format is LaTeX any text format may be used, e.g. this could be used to create a HTML webpage.

The template should be a standard LaTeX input file with the following tags included where the topo data should go.
\pn\ will replace these tags with the relevant information when creating the output LaTeX or pdf file.

There are two types of tags:
\begin{itemize}
 \item Repeating tags that consist of two matching tags.
       The text between the tags will be repeated with the relevant information added in each repetition.
       Note that the end tag should have an exclamation mark(!) before the tag name.
 \item Data tags that are replaced with the corresponding information.
\end{itemize}

The available tags are listed below.
These are case sensitive and could be empty.
It is recommended that the template includes a check where an empty tag could cause the compilation of the LaTeX file to fail or give undesired output.

\begin{description}
 \item [<{}<cragName>{}>] 
    The name of the crag.
    This tag can be anywhere in the template.
 \item [<{}<authors>{}>]
    The author(s) of the topo.
    This tag can be anywhere in the template.
 \item [<{}<textArea>{}> ... <{}<!textArea>{}>] 
    The text between these two tags will be replicated for each text area in topo (for example introduction sections).
 \begin{description}
    \item [<{}<textName>{}>]
        The title of the text area.
        This can only appear between \textbf{textArea} tags.
    \item [<{}<text>{}>]
        The body of the text area.
        This can only appear between \textbf{textArea} tags.
    \item [<{}<textImage>{}>] 
        The path to the image to include in the text area.
        Please note that this could be empty and will be relative to the folder containing the output file.
        This can only appear between \textbf{textArea} tags.
    \item [<{}<textCaption>{}>]
        The caption for the image.
        May also be empty.
        This can only appear between \textbf{textArea} tags.
 \end{description}
 \item [<{}<area>{}> ... <{}<!area>{}>] 
    The content between these two tags will be repeated for each area of the crag.
    \begin{description}
        \item [<{}<areaName>{}>] 
            This is the name of the area.
            This can only appear between \textbf{area} tags.
        \item [<{}<areaDescription>{}>]
            A description of the area.
            This can only appear between \textbf{area} tags.
        \item [<{}<img>{}> ... <{}<!img>{}>] 
            The content between these two tags will be repeated for each topo image for the area.
            If there are no images, the \textbf{img} tag content will be created once without an image.
            This can only appear between \textbf{area} tags.
        \begin{description}
            \item [<{}<image>{}>]
                The path to the topo image.
                Please note that this could be empty and will be relative to the folder containing the output file.
                This can only appear between \textbf{img} tags.
            \item [<{}<routes>{}> ... <{}<!routes>{}>] 
                The content between these two tags will be repeated for each route on the  topo image.
                This may also include routes that are not on an topo image.
                This can only appear between \textbf{img} tags.
            \begin{description}
                \item [<{}<Route-Project>{}>]
                    This tag is replaced with ``Route'' or ``Project'' depending on if the route is a project or not. 
                    This can only appear between \textbf{route} tags.
                \item [<{}<route-project>{}>]
                    This tag is replaced with ``route'' or ``project'' depending on if the route is a project or not. 
                    This can only appear between \textbf{route} tags.
                \item [<{}<name>{}>]
                    The name of the route.
                    This can only appear between \textbf{route} tags.
                \item [<{}<grade>{}>]
                    The grade of the route.
                    This can only appear between \textbf{route} tags.
                \item [<{}<description>{}>]
                    A description of the route.
                    This can only appear between \textbf{route} tags.
                \item [<{}<fa>{}>]
                    The first ascensionest of the route.
                    This can only appear between \textbf{route} tags.
                \item [<{}<faYear>{}>]
                    The year of first ascent.
                    This can only appear between \textbf{route} tags.
                \item [<{}<line>{}>] 
                    The line of the route in the asymptote format for drawing on the topo image.
                    The coordinate system of the line has (0,0) at the bottom left corner of the image and (1,1) in the top right corner.
                    See the example below for more detail on how this may be used.
                    This can only appear between \textbf{route} tags.
                \item [<{}<start>{}>]
                    The start point of the route using the same coordinate system as the \textbf{line} tag.
                    This can only appear between \textbf{route} tags.
                \item [<{}<end>{}>] 
                    The end point of the route using the same coordinate system as the \textbf{line} tag.
                    This can only appear between \textbf{route} tags.
            \end{description}
        \end{description}
    \end{description}
 \item [<{}<\%>{}>]
    A convenience tag that comments out the line in the template file.
    This allows the template file to be complied with out giving errors due to image files not being found or the input to commands not being correct.
    This is deleted from the outputted file.
\end{description}

\subsection{Example - Excerpt from the \emph{Default - Coloured Lines} Template}
\begin{verbatim}
\title{<<cragName>>}
\author{<<authors>>}

\begin{document}
    \maketitle

    \setcounter{secnumdepth}{0}
    \setcounter{tocdepth}{2}
    \newcounter{routeCount}
    \newcounter{routeCountTotal}

    <<textArea>>
        \section{<<textName>>}
        {\parskip 1.5ex %
            <<text>>
            \ifImage{<<textImage>>}{<<textCaption>>}
        }
    <<!textArea>>

    <<area>>
        \setcounter{routeCount}{0}   
        \setcounter{routeCountTotal}{0}  
        \orangeNext %This is so we start on red
        \ifsection{<<areaName>>}
        {\parskip 1.5ex %
            <<areaDescription>>
        }
        <<img>>
            \setResetColour
            \begin{figure}[h]
                \centering
                \begin{tikzpicture}
                    \node[anchor=south west,inner sep=0] (image) at (0,0) {
                        \includegraphics[width=\textwidth,height=0.5\textheight,keepaspectratio]{
                            <<image>>
                        }
                    };
                    \begin{scope}[x={(image.south east)},y={(image.north west)}]
                        <<routes>>
                            \draw<<Route-Project>>{<<line>>};
                        <<!routes>>
                        \setcounter{routeCount}{\therouteCountTotal}
                        \resetColour
                        <<routes>>
                            \draw<<Route-Project>>Ends{<<start>>}{<<end>>};
                        <<!routes>>
                    \end{scope}
                \end{tikzpicture}
            \end{figure}
            \begin{multicols}{2}
                \setcounter{routeCount}{\therouteCountTotal}      
                \resetColour
                <<routes>>
                    \<<route-project>>{<<name>>}{<<grade>>}{<<description>>}{<<fa>>}{<<faYear>>}
                <<!routes>>
            \end{multicols}
            \setcounter{routeCountTotal}{\therouteCount}  
        <<!img>>
        \FloatBarrier
    <<!area>>
\end{document}
\end{verbatim}
\end{document}
